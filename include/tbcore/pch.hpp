#ifndef TB_PCH_HPP__
#define TB_PCH_HPP__

#include <cstdlib>
#include <vector>
#include <list>
#include <map>
#include <string>

// base
#include "tbcore/predefined.hpp"
#include "tbcore/base/base.hpp"
#include "tbcore/base/string.hpp"
#include "tbcore/base/memory_buffer.hpp"
#include "tbcore/base/assert.hpp"
#include "tbcore/base/singleton_activator.hpp"
#include "tbcore/base/arbitrary_cast.hpp"
#include "tbcore/base/unique_id.hpp"

// refelction
#include "tbcore/reflection/type_cast.hpp"
#include "tbcore/reflection/variant.hpp"
#include "tbcore/reflection/lua_types.hpp"
#include "tbcore/reflection/type_cast.hpp"
#include "tbcore/reflection/property.hpp"
#include "tbcore/reflection/function.hpp"
#include "tbcore/reflection/type_info.hpp"
#include "tbcore/reflection/type_node.hpp"
#include "tbcore/reflection/reflection.hpp"
#include "tbcore/reflection/register.hpp"
#include "tbcore/reflection/type_traits.hpp"
#include "tbcore/reflection/object.hpp"
#include "tbcore/reflection/unique_object.hpp"
#include "tbcore/reflection/collection_enumer.hpp"

TB_NAMESPACE_BEGIN

using reflection::Property;
using reflection::Function;
using reflection::TypeNode;
using reflection::TypeInfo;
using reflection::ExtentInfo;
using reflection::CollectionEnumer;

TB_NAMESPACE_END

#endif // TB_PCH_HPP__
