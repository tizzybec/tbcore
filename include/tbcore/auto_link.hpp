#include "predefined.hpp"
#include "config.hpp"

#ifdef TB_LIB_NAME
# ifdef TB_USE_AUTO_LINK
#   pragma comment(lib, "libTB_" TB_STRINGIZE(TB_LIB_NAME) TB_DEBUG_FLAG "." TB_LIB_PREFFIX)
#   if defined(TB_LIB_DIAGNOSTIC)
#     pragma message("Linking to lib file: " "libTB_" TB_STRINGIZE(TB_LIB_NAME)TB_DEBUG_FLAG "." TB_LIB_PREFFIX)
#   endif 
# endif
#undef TB_LIB_NAME
#endif
