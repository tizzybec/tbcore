#ifndef TB_REFLECTION_JSON_OUTPUT_STREAM_H__
#define TB_REFLECTION_JSON_OUTPUT_STREAM_H__

#include "../buffer_traits.hpp"

TB_NAMESPACE_BEGIN

//refer: http://miloyip.github.io/rapidjson/md_doc_stream.html#CustomStream

template <class WriteBuffer>
class JsonOutputStream {
 public:
  BOOST_STATIC_ASSERT(is_write_buffer<WriteBuffer>::value);

  typedef char Ch;    //!< Character type of the stream.

  JsonOutputStream(WriteBuffer& wb) : wb_(wb) {};

  //! Begin writing operation at the current read pointer.
  //! \return The begin writer pointer.
  Ch* PutBegin() {
    return nullptr;
  }

  //! Write a character.
  void Put(Ch c) {
    wb_.WriteBytes(&c, sizeof(Ch));
  }

  //! Flush the buffer.
  void Flush() {}

  //! End the writing operation.
  //! \param begin The begin write pointer returned by PutBegin().
  //! \return Number of characters written.
  size_t PutEnd(Ch* begin) {
    boost::ignore_unused(begin)
    return static_cast<size_t>(wb_.WriteIndex());
  }

 private:
  //output stream should not implement read method

  //! Read the current character from stream without moving the read cursor.
  Ch Peek() const { return "\0"; }

  //! Read the current character from stream and moving the read cursor to next character.
  Ch Take() { return "\0"; }

  //! Get the current read cursor.
  //! \return Number of characters read from start.
  size_t Tell() { return 0; }

 private:
   WriteBuffer& wb_;
};

TB_NAMESPACE_END

#endif // #ifndef TB_REFLECTION_JSON_OUTPUT_STREAM_H__

