#ifndef TB_REFLECTION_DESERIALIZER_H__
#define TB_REFLECTION_DESERIALIZER_H__

#include <boost/core/enable_if.hpp>

#include "../reflection/variant.hpp"
#include "../reflection/type_traits.hpp"

TB_NAMESPACE_BEGIN

template <typename T, typename Enabled = void>
struct is_reader 
  : false_type {};

template <typename T> struct 
is_reader<T, typename enable_if<check_method<void (T::*)(Variant&), &T::ReadValue>::value >::type>
  : true_type {};

template <typename ReadBuffer, typename Reader>
class Deserializer {
 public:
  BOOST_STATIC_ASSERT(is_reader<Reader>::value);

  typedef ReadBuffer buffer_type;

  typedef Reader reader_type;
  
  static Variant Deserialize(ReadBuffer& rb) {
    reader_type r(rb);
    if (r.Parse()) {
      Variant v;
      r.ReadValue(v);
      return v;
    }
    return Variant();
  }
};

TB_NAMESPACE_END

#endif // TB_REFLECTION_DESERIALIZER_H__
