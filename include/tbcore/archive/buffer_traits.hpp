#ifndef TB_ARCHIVE_BUFFER_TRAITS_H__
#define TB_ARCHIVE_BUFFER_TRAITS_H__

#include "../base/result.hpp"
#include "../reflection/type_traits.hpp"

TB_NAMESPACE_BEGIN

template <typename T, typename Enabled = void>
struct is_write_buffer 
  : false_type{};

template <typename T> struct 
is_write_buffer<T, typename enable_if<check_method<void (T::*)(const char*, uint32), &T::WriteBytes>::value >::type>
  : true_type{};

template <typename T, typename Enabled = void>
struct is_read_buffer 
  : false_type{};

template <typename T> struct 
  is_read_buffer<T, typename enable_if<check_method<Result (T::*)(uint32, char*), &T::ReadBytes>::value >::type>
  : true_type{};

TB_NAMESPACE_END

#endif // TB_ARCHIVE_BUFFER_TRAITS_H__
