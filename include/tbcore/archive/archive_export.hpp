#ifndef TB_ARCHIVE_ARCHIVE_EXPORT_HPP__
#define TB_ARCHIVE_ARCHIVE_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_ARCHIVE_EXPORTS
# define TB_ARCHIVE_API TB_API_EXPORT
#else
# define TB_ARCHIVE_API TB_API_IMPORT
# define TB_LIB_NAME archive
# include "tbcore/auto_link.hpp"
#endif

#endif //TB_ARCHIVE_ARCHIVE_EXPORT_HPP__
