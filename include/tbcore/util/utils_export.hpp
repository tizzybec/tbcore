#ifndef TB_UTILS_EXPORT_HPP__
#define TB_UTILS_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_UTIL_EXPORTS
#else
# define TB_LIB_NAME util
# include "tbcore/auto_link.hpp"
#endif

#endif // TB_UTILS_EXPORT_HPP__
