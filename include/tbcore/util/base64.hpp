// util/base64.h

/*    Copyright 2009 10gen Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

#ifndef TB_UTIL_BASE64_H__
#define TB_UTIL_BASE64_H__

#include <cstring>
#include <iosfwd>
#include <string>

#include "utils_export.hpp"

TB_NAMESPACE_BEGIN

namespace base64 {

void encode(std::stringstream& ss , const char * data, int size);

std::string encode(const char* data , int size );

std::string encode(const std::string& s );

void decode(std::stringstream& ss , const std::string& s);

std::string decode(const std::string& s);

} //namespace base64

TB_NAMESPACE_END

#endif TB_UTIL_BASE64_H__
 
