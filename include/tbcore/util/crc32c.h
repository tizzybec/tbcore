#ifndef TB_UTIL_CRC32C_HPP_
#define TB_UTIL_CRC32C_HPP_

#include <stdint.h>

#include "utils_export.hpp"

#include "../base/basic_types.hpp"

TB_NAMESPACE_BEGIN

/*
    Computes CRC-32C (Castagnoli) checksum. Uses Intel's CRC32 instruction if it is available.
    Otherwise it uses a very fast software fallback.
*/
uint32_t crc32c_append(
    uint32_t crc,               // Initial CRC value. Typically it's 0.
                                // You can supply non-trivial initial value here.
                                // Initial value can be used to chain CRC from multiple buffers.
    const uint8_t *input,       // Data to be put through the CRC algorithm.
    size_t length);             // Length of the data in the input buffer.

TB_FORCEINLINE uint32 GetCRC32C(const char *input, size_t length) {
  return crc32c_append(0, (const uint8_t*)input, length);
}

TB_NAMESPACE_END

#endif //TB_UTIL_CRC32C_HPP_
