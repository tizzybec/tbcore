#ifndef digital_crc32_h__
#define digital_crc32_h__

#include <string>
#include "tbcore/base/basic_types.hpp"

TB_NAMESPACE_BEGIN

uint32 digital_update_crc32(uint32 crc, const uchar* buf, std::size_t len);

uint32 digital_update_crc32(uint32 crc, const std::string& buf);

uint32 digital_crc32(const uchar* buf, std::size_t len);

uint32 digital_crc32(const std::string& buf);

TB_NAMESPACE_END

#endif // digital_crc32_h__
