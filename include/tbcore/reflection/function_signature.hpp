#ifndef TB_REFLECTION_FUNCTOR_SIGNATURE_HPP__
#define TB_REFLECTION_FUNCTOR_SIGNATURE_HPP__

TB_NAMESPACE_BEGIN

template <typename T1, typename T2> 
struct FunctionSignature {
  typedef NoneSignature static_signature;
};

template <typename T1, typename ClassType, typename RetType, typename... ParamTypes>
struct FunctionSignature<T1, RetType (ClassType::*)(ParamTypes...)> {
  typedef VariadicsMemSignature<ParamTypes...> static_signature;
};

template <typename T1, typename ClassType, typename RetType, typename... ParamTypes>
struct FunctionSignature<T1, RetType (*)(ParamTypes...)> {
  typedef VariadicsRawSignature<ParamTypes...> static_signature;
};

TB_NAMESPACE_END

#endif // TB_REFLECTION_FUNCTOR_SIGNATURE_HPP__
