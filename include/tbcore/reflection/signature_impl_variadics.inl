#if defined(TB_REFELCTION_SIGNATURE_HPP__) && defined(TB_IMPLEMENT_SIGNATURE_VARIADICS)

#define __FUN_CONCAT_ACTUAL( A, B ) A##B
#define __FUN_CONCAT( A, B ) __FUN_CONCAT_ACTUAL( A, B )

#define TB_VARIADICS_CLASS_NAME __FUN_CONCAT(__TB_VARIADICS_CLASS_PREFIX, VariadicsSignature)

template <typename RetType __TB_PARAM_LIST>
struct  TB_VARIADICS_CLASS_NAME : Signature {
  uint32 GetParamCount() const OVERRITE {
    return __TB_PARAM_COUNT;
  }

  uint32 GetRetType() const OVERRITE {
    return GetVariantTypeId<RetType>();
  }

  uint32 GetParamType(uint32 index) const OVERRITE {
#ifndef __TB_NO_CASE
    switch (index) {
      __TB_GET_PARAM_TYPE_CASES
    }
#endif
    return 0;
  }
};

template <typename RetType __TB_PARAM_LIST>
Signature *MakeSignature() {
  return new TB_VARIADICS_CLASS_NAME<RetType __TB_PASS_PARAM>();
}

#undef TB_VARIADICS_CLASS_NAME

#undef __FUN_CONCAT_ACTUAL
#undef __FUN_CONCAT

#else
# error "This inline header must only be included by signature.h"
#endif
