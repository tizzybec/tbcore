#ifndef TB_REFLECTION_reflection_INFO_HPP__
#define TB_REFLECTION_reflection_INFO_HPP__

#include <boost/ptr_container/ptr_vector.hpp> //use for meta property container

#include "tbcore/base/mutex.hpp"

#include "operator.hpp"
#include "property.hpp"
#include "function.hpp"

TB_NAMESPACE_BEGIN

namespace reflection {

typedef boost::ptr_vector<Property> PropertiesType;
typedef boost::ptr_vector<Function> FunctionsType;

struct CollectionEnumer;

struct ExtentInfo {
	uint32 firstType;
	uint32 secondType;
	CollectionEnumer* iterator;
};

struct TypeInfo {
	uint32 id;
	uint32 parentId;
	uint32 interfaceId;
	uint32 interfaceImplId;
  String name;
	String nspace;
	String description;
  String inheritancePath;
	PropertiesType properties;
  FunctionsType functions;
	ConstPropertiesType inheritedProperties;
  ConstFunctionsType inheritedFunctions;
	bool initedInheritedProperties;
  bool initedInheritedFunctions;
	Operator operators;
	ExtentInfo* extInfo;
  SpinRWMutex mutex;
};

} //namepace reflection

TB_NAMESPACE_END

#endif //TB_REFLECTION_reflection_INFO_HPP__
