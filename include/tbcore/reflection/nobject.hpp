#ifndef TB_REFLECTION_NOBJECT_HPP__
#define TB_REFLECTION_NOBJECT_HPP__

#include "object.hpp"

TB_NAMESPACE_BEGIN

MSVC_PUSH_DISABLE_WARNING(4251)

class TB_REFLECTION_API NObject : public Object {
	DECLARE_reflection(NObject, Object);
public:
	NObject();
	NObject(const NObject& rhs);
	NObject& operator=(const NObject& rhs);
	~NObject();
	const String& GetName() const;
	void SetName(const String& name);
private:
	struct Data {
		String name_;
	} data_;
	struct NObjectImpl;
	_STD unique_ptr<NObjectImpl> impl_;
};

MSVC_POP_WARNING()

TB_NAMESPACE_END

#endif // TB_REFLECTION_NOBJECT_HPP__
