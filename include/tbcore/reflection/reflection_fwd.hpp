#ifndef TB_REFLECTION_reflection_FWD_HPP__
#define TB_REFLECTION_reflection_FWD_HPP__

#include "../base/base.hpp"

TB_NAMESPACE_BEGIN 

class Object;
class NObject;
class UniqueObject;
class UniqueId;
class Variant;

typedef std::pair<Variant, Variant> VariantPair;

TB_DECLARE_CLASS_PTR(Object);
TB_DECLARE_CLASS_PTR(NObject);
TB_DECLARE_CLASS_PTR(UniqueObject);

namespace reflection {

struct TypeInfo;
struct Function;
struct TypeNode;
struct Property;
struct Allocator;
struct CollectionEnumer;

typedef _STD vector<const Property*> ConstPropertiesType;
typedef _STD vector<const Function*> ConstFunctionsType;
typedef shared_ptr<struct CollectionEnumer> CollectionEnumerPtr;

} //namespace reflection

TB_NAMESPACE_END

#endif // TB_REFLECTION_reflection_FWD_HPP__
