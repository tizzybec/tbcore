#ifndef TB_REFLECTION_ALLOCATOR_H__
#define TB_REFLECTION_ALLOCATOR_H__

#include "allocator_traits.hpp"

TB_NAMESPACE_BEGIN

namespace reflection {

struct Allocator {
	virtual void* Malloc(const void* copy = nullptr) const = 0;
	virtual VoidPtr AssociatePtr(void* p) const = 0;
	virtual void Free(void* p) const = 0;
	virtual uint32 TypeSize() const = 0;
};

template <class T, class AllocatorTrait = DefaultAllocatorTrait<T> >
struct DefaultAllocator : Allocator {
	typedef AllocatorTrait allocator_trait;

	virtual void* Malloc(const void* copy) const {
		return allocator_trait::Malloc(copy);
	}

	virtual VoidPtr AssociatePtr(void* p) const {
		return shared_ptr<T>(static_cast<T*>(p));
	}

	virtual void Free(void* p) const {
		allocator_trait::Free(p);
	}

	virtual uint32 TypeSize() const {
		return sizeof(T);
	}
};

} //namespace reflection

TB_NAMESPACE_END

#endif // TB_REFLECTION_ALLOCATOR_H__
