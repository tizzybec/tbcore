#ifndef TB_REFLECTION_EXPORT_HPP__
#define TB_REFLECTION_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_REFLECTION_EXPORTS
# define TB_REFLECTION_API TB_API_EXPORT
#else
# define TB_REFLECTION_API TB_API_IMPORT
#   define TB_BASE_API TB_API_IMPORT
#   define TB_LIB_NAME reflection
#   include "tbcore/auto_link.hpp"
#endif

#endif //_TB_REFLECTION_EXPORT_HPP__
