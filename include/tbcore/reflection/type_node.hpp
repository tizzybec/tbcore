#ifndef TB_REFLECTION_reflection_NODE_HPP_
#define TB_REFLECTION_reflection_NODE_HPP_

#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/atomic/atomic.hpp>

#include "reflection_export.hpp"
#include "type_info.hpp"

TB_NAMESPACE_BEGIN

namespace reflection {

struct TypeNode {
	TypeInfo* typeInfo;
	TypeNode* parent;
	TypeNode* next;
	TypeNode* firstChild;
	boost::atomic_int count;
};

typedef boost::ptr_vector<TypeNode> reflectionNodeVector;

} //namespace reflection

TB_NAMESPACE_END

#endif //TB_REFLECTION_reflection_NODE_HPP_
