#ifndef TB_REFLECTION_UNIQUE_OBJECT_HPP__
#define TB_REFLECTION_UNIQUE_OBJECT_HPP__

#include "tbcore/base/unique_id.hpp"

#include "reflection_export.hpp"
#include "nobject.hpp"

TB_NAMESPACE_BEGIN

MSVC_PUSH_DISABLE_WARNING(4251)

class TB_REFLECTION_API UniqueObject : public NObject {
	DECLARE_reflection(UniqueObject, NObject);
public:
	 UniqueObject();
	 UniqueObject(const UniqueObject& rhs);
	 UniqueObject& operator=(const UniqueObject& rhs);
	 ~UniqueObject();
	 const UniqueId& GetObjectId() const;
	 void SetObjectId(const UniqueId& objId);

private:
	struct Data {
		UniqueId objId_;
	} data_;
	struct UniqueObjectImpl;
	_STD unique_ptr<UniqueObjectImpl> impl_;
};

MSVC_POP_WARNING()

TB_NAMESPACE_END

#endif // TB_REFLECTION_UNIQUE_OBJECT_HPP__
