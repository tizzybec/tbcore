#ifndef TB_REFLECTION_reflection_OPERATOR_HPP__
#define TB_REFLECTION_reflection_OPERATOR_HPP__

#include "reflection_export.hpp"
#include "reflection_fwd.hpp"

TB_NAMESPACE_BEGIN

namespace reflection {

struct Operator {
  const Function* defaultCtor;
  const Function* secondCtor;
  Allocator* allocator;
};

} //namepace reflection

TB_NAMESPACE_END

#endif //TB_REFLECTION_reflection_OPERATOR_HPP__
