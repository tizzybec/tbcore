#ifndef _TB_REFLECTION_FUNCTION_TRAITS_HPP_
#define _TB_REFLECTION_FUNCTION_TRAITS_HPP_

#include "tbcore/predefined.hpp"

TB_NAMESPACE_BEGIN

template <typename FunctorType>
struct  function_class_type {
  typedef NoneType type;
};

template <typename ClassType, typename RetType, typename... Args>
struct  function_class_type<RetType (ClassType::*)(Args...)> {
  typedef ClassType type;
};

TB_NAMESPACE_END

#endif //_TB_REFLECTION_FUNCTION_TRAITS_HPP_
