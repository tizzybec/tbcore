#ifndef TB_DB_DB_FWD_HPP__
#define TB_DB_DB_FWD_HPP__

#include "tbcore/base/base.hpp"

TB_NAMESPACE_BEGIN

struct DBInterface;
struct DBiterator;
typedef shared_ptr<struct DBiterator> DBIteratorPtr;

TB_NAMESPACE_END

#endif // TB_DB_DB_FWD_HPP__