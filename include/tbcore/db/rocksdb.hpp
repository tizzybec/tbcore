#ifndef TB_DB_ROCKSDB_HPP__
#define TB_DB_ROCKSDB_HPP__

#include "db_interface.hpp"

TB_NAMESPACE_BEGIN

class TB_DB_API RocksDB : NON_EXPORTED_BASE(public DBInterface) {
 public:
	RocksDB();
	~RocksDB();
	Result OpenDB(const String& param, bool createIfNotExist = true, 
		bool readOnly = false) OVERRITE;
	Result CloseDB() OVERRITE;
	Result DestroyDB(const String& param) OVERRITE;
	Result Write(const Blob& key, const char* val, uint32 vallen) OVERRITE;
	Result WriteValue(const Blob& key, const Variant& val) OVERRITE;
	Result WriteBatch(const Blob* key, const Variant* vals, uint32 len) OVERRITE;
	void Flush() OVERRITE;
	Result Read(const Blob& key, char** val, uint32& vallen) OVERRITE;
	Result ReadValue(const Blob& key, Variant& val) OVERRITE;
	DBIteratorPtr ReadFilter(DBFilterInterface* filter) OVERRITE;
	Result ReadBatch(const Blob* keys, uint32 keylen,
		char** vallist, size_t& valsizelist) OVERRITE;
	DBIteratorPtr ReadByRange(const Blob& minkey, const Blob& maxkey, 
    const _STD string& prefix = _STD string(), RangeProcT pred = DefaultKeyPred) OVERRITE;
	Result Delete(const Blob& key) OVERRITE;
	Result DeleteFilter(DBFilterInterface* filter, bool onlyOnce) OVERRITE;
	Result DeleteBatch(const Blob* keys, uint32 keylen) OVERRITE;
	Result DeleteByRange(const Blob& minkey, const Blob& maxkey, 
		RangeProcT pred = nullptr) OVERRITE;
 private:
	struct RocksDBImpl;
	RocksDBImpl* impl_;
};

TB_NAMESPACE_END

#endif // TB_DB_ROCKSDB_HPP__
