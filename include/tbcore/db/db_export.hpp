#ifndef TB_DB_DB_EXPORT_HPP__
#define TB_DB_DB_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_DB_EXPORTS
# define TB_DB_API TB_API_EXPORT
#else
# define TB_DB_API TB_API_IMPORT
# define TB_LIB_NAME db
# include "tbcore/auto_link.hpp"
#endif

#endif // TB_DB_DB_EXPORT_HPP__
