#ifndef TB_BASE_LIBRARY_LOADER_HPP__
#define TB_BASE_LIBRARY_LOADER_HPP__

#include "base_export.hpp"

#ifdef TB_OS_WIN
#include <WTypes.h>
#endif // TB_OS_WIN

TB_NAMESPACE_BEGIN

#ifdef TB_OS_WIN 
typedef HMODULE LibraryHandle;
typedef FARPROC SymbolAddress;
#else
typedef void* LibraryHandle;
typedef intptr_t (*SymbolAddress)(void);
#endif 

struct TB_BASE_API LibraryLoader {

	static LibraryHandle Load(const TCHAR* path);

	static bool Free(LibraryHandle handle);

	static SymbolAddress GetSymbol(LibraryHandle handle, const TCHAR *sym);

	static bool IsLoaded(const TCHAR* path);
};

TB_NAMESPACE_END

#endif // TB_BASE_LIBRARY_LOADER_HPP__
