#ifndef TB_BASE_FILE_UTIL_HPP__
#define TB_BASE_FILE_UTIL_HPP__

#include "base_export.hpp"
#include "string.hpp"
#include "result.hpp"
#include "memory_buffer.hpp"

TB_NAMESPACE_BEGIN

class TB_BASE_API File {
public:
  typedef enum {
    kUnknownFileType,
    kBinaryFile,            //bin
    kExecutionFile,         //exe
    kDynamicLibrary,        //dll;so
    kStaticLibrary,         //a;lib;dylib
    kTextDocument,          //txt
    kMarkdownDocument,      //md;markdown
    kXmlDocument,           //xml
    kHtmlDocument,          //htm;html;xhtml
    kJsonDocument,          //json
    kWordDocument,          //doc;docx
    kPPTDocument,           //ppt;pptx
    kXpsDocument,           //xps
    kVisioDocument,         //visio
    kIniDocument,           //ini
    kSourceCode,            //i;h;hpp;hxx;hm;inl;inc;xsd;cpp;c;cc;cxx;hpj;asm;asmx
    kBatchFile,             //bat;cmd;ps
    kScriptFile,            //py;rb;vb;js;lua
    kScriptLua,             //lua
  } FileType;

  static  bool IsBinaryFile(const String& filename);
  static  bool IsExecutionFile(const String& filename);
  static  bool IsDynamicLibrary(const String& filename);
  static  bool IsStaticLibrary(const String& filename);
  static  bool IsTextDocument(const String& filename);
  static  bool IsMarkdownDocument(const String& filename);
  static  bool IsXmlDocument(const String& filename);
  static  bool IsHtmlDocument(const String& filename);
  static  bool IsJsonDocument(const String& filename);
  static  bool IsWordDocument(const String& filename);
  static  bool IsPPTDocument(const String& filename);
  static  bool IsXpsDocument(const String& filename);
  static  bool IsVisioDocument(const String& filename);
  static  bool IsIniDocument(const String& filename);
  static  bool IsSourceCode(const String& filename);
  static  bool IsBatchFile(const String& filename);
  static  bool IsScriptLua(const String& filename);
  static  bool IsScriptFile(const String& filename);

  static  FileType GetFileType(const String& filename);
  static  String GetFileExtension(const String& filename);
  static  String GetFileFullExtension(const String& filename);

  static  Result WriteBinary(const String &path, MemoryBuffer& data);
  static  Result ReadBinary(const String &path, MemoryBuffer& data);
};


TB_NAMESPACE_END

#endif // TB_BASE_FILE_UTIL_HPP__
