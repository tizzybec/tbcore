#ifndef TB_BASE_SCOPED_CLEAR_ERRNO_H__
#define TB_BASE_SCOPED_CLEAR_ERRNO_H__

#include <errno.h>
#include "base.hpp"

TB_NAMESPACE_BEGIN

class ScopedClearErrno {
public:
  ScopedClearErrno() : oldErrno_(errno) { errno = 0; }
  ~ScopedClearErrno() { if (errno == 0) errno = oldErrno_; }
private:
  TB_DISABLE_COPY_AND_ASSIGN(ScopedClearErrno);
  const int oldErrno_;
};

TB_NAMESPACE_END

#endif  // TB_BASE_SCOPED_CLEAR_ERRNO_H__
