#ifndef TB_BASE_DURATION_HPP__
#define TB_BASE_DURATION_HPP__

#include <boost/chrono.hpp>

#include "base_export.hpp"
#include "basic_types.hpp"

TB_NAMESPACE_BEGIN

template class TB_BASE_API boost::chrono::duration<int64, boost::milli>;

class TB_BASE_API Duration {
public:
  typedef int64 rep_type;

  typedef boost::milli period_type;

  typedef boost::chrono::duration<rep_type, period_type> value_type;

  Duration();
  Duration(int64 milliseconds);
  explicit Duration(const value_type& d);
  Duration(const Duration& rhs);
  Duration& operator=(const Duration& rhs);
  ~Duration();

  double TotalSeconds() const;
  int64 TotalMilliseconds() const;
  void Reset(int64 milliseconds = 0);
  rep_type Compare(const Duration& rhs) const;
  const value_type &Value() const;
  bool IsZero() const;
  bool IsPositive() const;
	bool IsNegtive() const;

  Duration &operator+=(const Duration& rhs);
  Duration &operator-=(const Duration& rhs);
  Duration &operator*=(rep_type n);
  Duration &operator/=(rep_type n);
  Duration operator+(const Duration& rhs) const;
  Duration operator-(const Duration& rhs) const;
  Duration operator*(rep_type n) const;
  Duration operator/(rep_type n) const;

  static Duration FromSeconds(uint64 seconds);
  static Duration Min();
  static Duration Max();
  static Duration Zero();
 private:
  value_type value_;
};

#define TB_IMPLEMENT_DURATION_COMPARATION(op) \
 inline bool operator##op##(const Duration& lhs, const Duration& rhs) {\
  return lhs.Compare(rhs) ##op## 0;\
}

TB_IMPLEMENT_DURATION_COMPARATION(<);
TB_IMPLEMENT_DURATION_COMPARATION(<=);
TB_IMPLEMENT_DURATION_COMPARATION(>);
TB_IMPLEMENT_DURATION_COMPARATION(>=);
TB_IMPLEMENT_DURATION_COMPARATION(!=);
TB_IMPLEMENT_DURATION_COMPARATION(==);

#undef TB_IMPLEMENT_DURATION_COMPARATION

TB_NAMESPACE_END

#endif //TB_BASE_DURATION_HPP__
