#ifndef TB_BASE_BASE_FWD_HPP__
#define TB_BASE_BASE_FWD_HPP__

#include "base.hpp"

TB_NAMESPACE_BEGIN

class SmartAssert;
struct MemoryBufferImpl;

typedef shared_ptr<class MemoryBuffer> MemoryBufferPtr;
typedef weak_ptr<struct AsyncLoggerInterface> AsyncLoggerInterfaceWPtr;
typedef shared_ptr<struct AsyncLoggerInterface> AsyncLoggerInterfacePtr;

TB_NAMESPACE_END

#endif // TB_BASE_BASE_FWD_HPP__
