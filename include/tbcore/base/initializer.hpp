#ifndef TB_BASE_INITIALIZER_HPP__
#define TB_BASE_INITIALIZER_HPP__

#include "result.hpp"
#include "unordered.hpp"

TB_NAMESPACE_BEGIN

 struct InitializerContext {

 	typedef function<Result (const InitializerContext& context)> InitializerFunction;
 
 	typedef std::vector<std::string> ArgumentVector;
 
 	typedef unordered_map<_STD string, _STD string> EnvironmentMap;
 
 	ArgumentVector args;

 	EnvironmentMap env;
 };

 struct IntializerGraph {
 	struct IntializerGraphNode {
 		InitializerContext::InitializerFunction func;
 		unordered_set<_STD string> prerequisites;
 	};

	typedef unordered_map<_STD string, IntializerGraphNode> NodesMapT;

	NodesMapT nodes;
 };

class TB_BASE_API Initializer {
public:
 	Initializer();
 	~Initializer();
 	void RegisterInitializer(
 		const _STD string &name,
 		const InitializerContext::InitializerFunction &fn,
 		const std::vector<_STD string>& prerequisites,
 		const std::vector<_STD string>& depends);
 	Result Execute(const InitializerContext& contex);

private:
 	void SortByPrerequistes(_STD vector<_STD pair<_STD string, int> >& sortedNodes);
 	IntializerGraph graph;
 	TB_DISABLE_COPY_AND_ASSIGN(Initializer);
 };
 
 TB_NAMESPACE_END

#endif // TB_BASE_INITIALIZER_HPP__
