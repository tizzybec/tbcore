#ifndef TB_BASE_STRING_H__
#define TB_BASE_STRING_H__

#include <sstream>
#include <fstream>

#include "string/string16.hpp"

TB_NAMESPACE_BEGIN

#if defined(TCHAR)
# undef TCHAR
#endif

#if defined(TB_UNICODE)
typedef TB_NAMESPACE::string16 String;
typedef TB_NAMESPACE::char16 TCHAR;
#else
typedef std::string String
typedef char TCHAR;
#endif

typedef _STD basic_ostream<TB_NAMESPACE::char16, _STD char_traits<TB_NAMESPACE::char16>> WOStream;
typedef _STD basic_stringstream < TB_NAMESPACE::char16, _STD char_traits<TB_NAMESPACE::char16>,\
  _STD allocator<TB_NAMESPACE::char16> > WStringStream;
typedef _STD basic_stringstream<TCHAR, _STD char_traits<TCHAR>,\
  _STD allocator<TCHAR> > YSStringStream;
typedef _STD basic_ofstream<TCHAR, _STD char_traits<TCHAR> > OFStream;

TB_NAMESPACE_END

#endif // TB_BASE_STRING_H__
