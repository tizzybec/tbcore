#ifndef TB_BASE_ERROR_CODE_HPP__
#define TB_BASE_ERROR_CODE_HPP__

#include "base.hpp"
#include "string.hpp"
#include "base_export.hpp"

TB_NAMESPACE_BEGIN

enum ErrorCode {
	kNoError = 0,
  kUnknownError,
	kNullObjectId,
	kObjectExists,
	kObjectNotExists,
	kRunOutOfData,
	kGeoConvertFailed,
	kOpenDatabaseFailed,
	kDestroyDatabaseFailed,
	kReadDatabaseFailed,
	kDatabaseIteratorEnd,
	kWriteDatabaseFailed,
	kInvalidDatebase,
	kErrorNewSocket,
	kErrorConnectSocket,
	kOpenFileFlaied,
	kWriteFileFailed,
	kModuleEixsts,
  kServiceRegistered,
  kServiceNotAvailable,
  kTerminalNotExist,
  kInvalidBrokerPkg,
  kRecordPathNotSupplied,
  kErrorEvalLuaScript,
  kInvalidJsonInput,
  kVerifyFailed,
  kUserErrorCode,
};

TB_BASE_API String ErrorCode2String(ErrorCode& errCode);
TB_BASE_API int String2ErrorCode(const String& errCodeStr);

TB_BASE_API const int RegisterErrorCode(const String& errCodeStr, const String& errDetail);
TB_BASE_API String GetErrorDetail(const String& errCodeStr, const String& errDetail);
TB_BASE_API String GetErrorDetail(int errCode, const String& errDetail);

TB_NAMESPACE_END

#endif // TB_BASE_ERROR_CODE_HPP__
