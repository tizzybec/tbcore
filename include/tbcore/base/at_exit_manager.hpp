#ifndef CORE_AT_EXIT_MANAGER_HPP_
#define CORE_AT_EXIT_MANAGER_HPP_

#include "base_export.hpp"
#include "base.hpp"

TB_NAMESPACE_BEGIN

class TB_BASE_API AtExitManager {
public:
  AtExitManager();
  ~AtExitManager();

  typedef function<void(void)> ExitFunctorType;

  static void RegisterAtExit(const ExitFunctorType& func);
  static void ProcessExitNow();

private:
  TB_DISABLE_COPY_AND_ASSIGN(AtExitManager);
  struct AtExitManagerImpl;
  AtExitManagerImpl* impl_;
};

TB_NAMESPACE_END

#endif //CORE_AT_EXIT_MANAGER_HPP_ 
