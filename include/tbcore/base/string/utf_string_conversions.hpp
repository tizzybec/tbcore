// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef BASE_STRINGS_UTF_STRING_CONVERSIONS_H_
#define BASE_STRINGS_UTF_STRING_CONVERSIONS_H_

#include <string>

#include "tbcore/base/base_export.hpp"
#include "tbcore/base/string/string16.hpp"
#include "tbcore/base/string/string_piece.hpp"

TB_NAMESPACE_BEGIN

// These convert between UTF-8, -16, and -32 strings. They are potentially slow,
// so avoid unnecessary conversions. The low-level versions return a boolean
// indicating whether the conversion was 100% valid. In this case, it will still
// do the best it can and put the result in the output buffer. The versions that
// return strings ignore this error and just return the best conversion
// possible.
TB_BASE_API bool WideToUTF8(const wchar_t* src, size_t src_len,
                            std::string* output);
TB_BASE_API std::string WideToUTF8(const std::wstring& wide);
TB_BASE_API bool UTF8ToWide(const char* src, size_t src_len,
                            std::wstring* output);
TB_BASE_API std::wstring UTF8ToWide(const StringPiece& utf8);

TB_BASE_API bool WideToUTF16(const wchar_t* src, size_t src_len,
                             string16* output);
TB_BASE_API string16 WideToUTF16(const std::wstring& wide);
TB_BASE_API bool UTF16ToWide(const char16* src, size_t src_len,
                             std::wstring* output);
TB_BASE_API std::wstring UTF16ToWide(const string16& utf16);

TB_BASE_API bool UTF8ToUTF16(const char* src, size_t src_len, string16* output);
TB_BASE_API string16 UTF8ToUTF16(const StringPiece& utf8);
TB_BASE_API bool UTF16ToUTF8(const char16* src, size_t src_len,
                             std::string* output);
TB_BASE_API std::string UTF16ToUTF8(const string16& utf16);

// This converts an ASCII string, typically a hardcoded constant, to a UTF16
// string.
TB_BASE_API string16 ASCIIToUTF16(const StringPiece& ascii);

// Converts to 7-bit ASCII by truncating. The result must be known to be ASCII
// beforehand.
TB_BASE_API std::string UTF16ToASCII(const string16& utf16);

TB_NAMESPACE_END

#endif  // BASE_STRINGS_UTF_STRING_CONVERSIONS_H_
