// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef BASE_STRINGS_SYS_STRING_CONVERSIONS_H_
#define BASE_STRINGS_SYS_STRING_CONVERSIONS_H_

// Provides system-dependent string type conversions for cases where it's
// necessary to not use ICU. Generally, you should not need this in Chrome,
// but it is used in some shared code. Dependencies should be minimal.

#include <string>

#include "tbcore/base/base_export.hpp"
#include "tbcore/base/basic_types.hpp"
#include "tbcore/base/string/string16.hpp"
#include "tbcore/base/string/string_piece.hpp"

#if defined(TB_OS_MACOSX)
#include <CoreFoundation/CoreFoundation.h>
#ifdef __OBJC__
@class NSString;
#else
class NSString;
#endif
#endif  // OS_MACOSX

TB_NAMESPACE_BEGIN

// Converts between wide and UTF-8 representations of a string. On error, the
// result is system-dependent.
TB_BASE_API std::string SysWideToUTF8(const std::wstring& wide);
TB_BASE_API std::wstring SysUTF8ToWide(const StringPiece& utf8);

// Converts between wide and the system multi-byte representations of a string.
// DANGER: This will lose information and can change (on Windows, this can
// change between reboots).
TB_BASE_API std::string SysWideToNativeMB(const std::wstring& wide);
TB_BASE_API std::wstring SysNativeMBToWide(const StringPiece& native_mb);

// Windows-specific ------------------------------------------------------------

#if defined(TB_OS_WIN)

// Converts between 8-bit and wide strings, using the given code page. The
// code page identifier is one accepted by the Windows function
// MultiByteToWideChar().
TB_BASE_API std::wstring SysMultiByteToWide(const StringPiece& mb,
                                            uint32 code_page);
TB_BASE_API std::string SysWideToMultiByte(const std::wstring& wide,
                                           uint32 code_page);

#endif  // defined(TB_OS_WIN)

// Mac-specific ----------------------------------------------------------------

#if defined(TB_OS_MACOSX)

// Converts between STL strings and CFStringRefs/NSStrings.

// Creates a string, and returns it with a refcount of 1. You are responsible
// for releasing it. Returns NULL on failure.
TB_BASE_API CFStringRef SysUTF8ToCFStringRef(const std::string& utf8);
TB_BASE_API CFStringRef SysUTF16ToCFStringRef(const string16& utf16);

// Same, but returns an autoreleased NSString.
TB_BASE_API NSString* SysUTF8ToNSString(const std::string& utf8);
TB_BASE_API NSString* SysUTF16ToNSString(const string16& utf16);

// Converts a CFStringRef to an STL string. Returns an empty string on failure.
TB_BASE_API std::string SysCFStringRefToUTF8(CFStringRef ref);
TB_BASE_API string16 SysCFStringRefToUTF16(CFStringRef ref);

// Same, but accepts NSString input. Converts nil NSString* to the appropriate
// string type of length 0.
TB_BASE_API std::string SysNSStringToUTF8(NSString* ref);
TB_BASE_API string16 SysNSStringToUTF16(NSString* ref);

#endif  // defined(TB_OS_MACOSX)

TB_NAMESPACE_END

#endif  // BASE_STRINGS_SYS_STRING_CONVERSIONS_H_
