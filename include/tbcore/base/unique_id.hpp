#ifndef TB_BASE_OBJECT_ID_H__
#define TB_BASE_OBJECT_ID_H__

#include <string>

#include "tbcore/base/basic_types.hpp"

#include "base_export.hpp"

TB_NAMESPACE_BEGIN

class TB_BASE_API UniqueId {
public:
  enum {
    kUniqueIdSize = 12,
		kTimestampSize = 4,
		kInstanceUniqueSize = 5,
		kIncrementSize = 3
  };

  struct TB_BASE_API Hasher {
	  size_t operator() (const UniqueId& oid) const;
  };

  UniqueId();
  explicit UniqueId(const std::string& idStr);
  explicit UniqueId(const uint8 (&arr)[kUniqueIdSize]);
  UniqueId(const UniqueId& rhs);
  UniqueId& operator=(const UniqueId& rhs);
  ~UniqueId();
	bool IsNull() const;
  int Compare(const UniqueId& rhs) const;
  std::string ToString() const;
	void HashCombine(std::size_t seed) const;
  char* Data() const;
	const char* ConstData() const;
  static UniqueId GenUID();
	static void Fork();
private:
	uint8 data_[kUniqueIdSize];
};

#define TB_IMPLEMENT_UNIQUE_ID_COMPARATION(op) \
inline bool operator##op##(const UniqueId& lhs, const UniqueId& rhs) { \
	return lhs.Compare(rhs) ##op## 0; \
}

TB_IMPLEMENT_UNIQUE_ID_COMPARATION(<);
TB_IMPLEMENT_UNIQUE_ID_COMPARATION(<=);
TB_IMPLEMENT_UNIQUE_ID_COMPARATION(>);
TB_IMPLEMENT_UNIQUE_ID_COMPARATION(>=);
TB_IMPLEMENT_UNIQUE_ID_COMPARATION(!=);
TB_IMPLEMENT_UNIQUE_ID_COMPARATION(==);

#undef TB_IMPLEMENT_UNIQUE_ID_COMPARATION

TB_NAMESPACE_END

#endif // TB_BASE_OBJECT_ID_H__
