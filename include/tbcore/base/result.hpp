#ifndef TB_BASE_RESULT_HPP__
#define TB_BASE_RESULT_HPP__

#include "base.hpp"
#include "string.hpp"
#include "base_export.hpp"
#include "error_code.hpp"
#include "basic_types.hpp"

TB_NAMESPACE_BEGIN

MSVC_PUSH_DISABLE_WARNING(4251)
class TB_BASE_API Result {
public:
	 Result();
	 Result(ErrorCode code, const String& reason = _T(""), int32 location = 0);
	 Result(const Result& rhs);
   Result(Result&& rhs);
	 Result& operator=(const Result& rhs);
   Result& operator=(Result&& rhs);
	 operator bool() const;
	 ~Result();
	 bool HasError() const;
	 ErrorCode Code() const;
	 const String& Reason() const;
	 int32 Location() const;
	 String ToString() const;
private:
	struct ResultInfo {
		ResultInfo(ErrorCode c, const String& r, int32 l)
		: code_(c), reason_(r), location_(l) {}
		ErrorCode code_;
		String reason_;
		int32 location_;
	};
	shared_ptr<ResultInfo> info_;
};
MSVC_POP_WARNING()

TB_NAMESPACE_END

#endif // TB_BASE_RESULT_HPP__
