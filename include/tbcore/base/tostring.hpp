#ifndef TB_BASE_TOSTRING_H__
#define TB_BASE_TOSTRING_H__

#include "string/utf_string_conversions.hpp"

TB_NAMESPACE_BEGIN

struct ToStringImpl {
  template <typename To, typename From>
  static To ToString(const From& str) {
    return str;
  }

  template <>
  static std::string ToString(const string16& str) {
    return TB_NAMESPACE::UTF16ToUTF8(str);
  }

  template <>
  static string16 ToString(const std::string& str) {
    return TB_NAMESPACE::UTF8ToUTF16(str);
  }
};

struct ToAsciiStringImpl {
	template <typename From>
	static std::string ToAsciiString(const From& str) {
		return std::string();
	}

	template <>
	static std::string ToAsciiString(const std::string& str) {
		return str;
	}

	template <>
	static std::string ToAsciiString(const string16& str) {
		return TB_NAMESPACE::UTF16ToUTF8(str);
	}
};

TB_NAMESPACE_END

#define TB_STRING(str) \
  (TB_NAMESPACE::ToStringImpl::ToString<TB_NAMESPACE::String>(str))

#define TB_ASCII_STRING(str) \
  (TB_NAMESPACE::ToAsciiStringImpl::ToAsciiString(str))

#endif // TB_BASE_TOSTRING_H__
