#ifndef TB_BASE_MAKE_CONTAINERS_HPP__
#define TB_BASE_MAKE_CONTAINERS_HPP__

#include "base.hpp"
#include "base_export.hpp"

TB_NAMESPACE_BEGIN

namespace detail {

TB_BASE_API std::vector<std::string> MakeStringVector(int ignored, ...);

}

TB_NAMESPACE_END

#define TB_MAKE_STRING_VECTOR(...) TB_NAMESPACE::detail::MakeStringVector(0, __VA_ARGS__, NULL)

#endif // TB_BASE_MAKE_CONTAINERS_HPP__
