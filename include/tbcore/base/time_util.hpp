#ifndef TB_BASE_TIME_UTIL_HPP__
#define TB_BASE_TIME_UTIL_HPP__

#include "base_export.hpp"
#include "basic_types.hpp"

TB_NAMESPACE_BEGIN

TB_BASE_API void MillisecsToHMS(
  int64 millisecs, 
  int32& hours, 
  int32& minutes, 
  int32& secs, 
  int32& millis);

TB_NAMESPACE_END

#endif // TB_BASE_TIME_UTIL_HPP__
