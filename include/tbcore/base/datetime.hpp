#ifndef CORE_DATE_TIME_HPP_
#define CORE_DATE_TIME_HPP_

#include <boost/chrono.hpp>

#include "base_export.hpp"
#include "duration.hpp"

namespace TB {

template class TB_BASE_API boost::chrono::time_point<boost::chrono::system_clock, Duration::value_type>;

class TB_BASE_API DateTime {
 public:
  typedef boost::chrono::system_clock clock_type;

  typedef Duration::value_type duration_type;

  typedef boost::chrono::time_point<clock_type, duration_type> value_type;

  DateTime();
  DateTime(int64 milliseconds);
  explicit DateTime(const value_type& d);
  explicit DateTime(const Duration& d);
  explicit DateTime(const duration_type& d);
  DateTime(const DateTime& rhs);
  DateTime& operator=(const DateTime& rhs);
  ~DateTime();
  int Compare(const DateTime& rhs) const;
  int64 TotalMilliseconds() const;
	double TotalSeconds() const;
 
  DateTime& operator+=(const Duration& d);
  DateTime& operator-=(const Duration& d);
  DateTime operator+(const Duration& d) const;
  DateTime operator-(const Duration& d) const;
  Duration operator-(const DateTime& rhs) const;
  Duration TimeSinceEpoch();

  static DateTime FromTimeT(std::time_t t);
  static DateTime Now() NOEXCEPT;
  static DateTime Min();
  static DateTime Max();
 private:
  value_type value_;
};

#define TB_IMPLEMENT_DATETIME_COMPARATION(op) \
inline bool operator##op##(const DateTime& lhs, const DateTime& rhs) {\
  return lhs.Compare(rhs) ##op## 0;\
}

TB_IMPLEMENT_DATETIME_COMPARATION(<);
TB_IMPLEMENT_DATETIME_COMPARATION(<=);
TB_IMPLEMENT_DATETIME_COMPARATION(>);
TB_IMPLEMENT_DATETIME_COMPARATION(>=);
TB_IMPLEMENT_DATETIME_COMPARATION(!=);
TB_IMPLEMENT_DATETIME_COMPARATION(==);

#undef TB_IMPLEMENT_DATETIME_COMPARATION

} //namespace TB

#endif //CORE_DATE_TIME_HPP_
