#ifndef CORE_SINGLETON_ACTIVATOR_HPP__
#define CORE_SINGLETON_ACTIVATOR_HPP__

#include "tbcore/predefined.hpp"

TB_NAMESPACE_BEGIN

template <class T>
class SingletonActivator {
 public:
  static T &Activate() {
    Use(ins_);
    static T a;
    return a;
  }
 private:
  static void Use(const T &x) { (void)x; };
  static T &ins_;
};

template <class T>
T &SingletonActivator<T>::ins_ = SingletonActivator<T>::Activate();

TB_NAMESPACE_END

#endif //CORE_SINGLETON_ACTIVATOR_HPP__
