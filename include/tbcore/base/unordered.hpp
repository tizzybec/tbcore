#ifndef TB_BASE_UNORDERED_HPP__
#define TB_BASE_UNORDERED_HPP__

#include "tbcore/config.hpp"

#ifdef TB_CXX_PREFER_STD_LIB
#include <unordered_map>
#include <unordered_set>
TB_NAMESPACE_BEGIN
using _STD unordered_map;
using _STD unordered_set;
TB_NAMESPACE_END
#else 
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
TB_NAMESPACE_BEGIN
using ::boost::unordered_map;
using ::boost::unordered_set;
TB_NAMESPACE_END
#endif

#endif // TB_BASE_UNORDERED_HPP__
