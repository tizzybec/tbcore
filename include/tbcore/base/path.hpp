#ifndef TB_BASE_PATH_UTIL_HPP__
#define TB_BASE_PATH_UTIL_HPP__

#include <vector>

#include "base_export.hpp"
#include "string.hpp"

TB_NAMESPACE_BEGIN

class TB_BASE_API Path {
public:
  static void Walk(const String &path,
    std::vector<String> &files, int depth = -1) NOEXCEPT;

  static void WalkWithExt(const String &path, const TCHAR *ext,
    std::vector<String> &files, int depth = -1) NOEXCEPT;

  static bool IsDir(const String &path) NOEXCEPT;

  static bool IsExists(const String &path) NOEXCEPT;

  static String Parent(const String &path) NOEXCEPT;

  static String ToNative(const String &path) NOEXCEPT;
};


TB_NAMESPACE_END

#endif // TB_BASE_PATH_UTIL_HPP__
