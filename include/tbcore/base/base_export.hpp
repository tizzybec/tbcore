#ifndef TB_BASE_BASE_EXPORT_HPP__
#define TB_BASE_BASE_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_BASE_EXPORTS
# define TB_BASE_API TB_API_EXPORT
#else
# define TB_BASE_API TB_API_IMPORT
# define TB_LIB_NAME base
# include "tbcore/auto_link.hpp"
#endif

#endif //TB_BASE_BASE_EXPORT_HPP__
