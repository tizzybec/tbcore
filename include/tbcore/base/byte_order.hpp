#ifndef TB_BASE_BYTE_ORDER_HPP__
#define TB_BASE_BYTE_ORDER_HPP__

TB_NAMESPACE_BEGIN

template <typename T>
T ToLittleEndian(T v) {
#if defined(TB_ARCH_ENDIAN_LITTLE)
  return v;
#else
  T r;
  uint32 s = sizeof(T);
  char* data = (char*)&v;
  char* rdata = (char*)&r;
  for (int i = s - 1; i >= 0; i--) {
    *(rdata + (s - 1 - i)) = *(data + i);
  }
  return r;
#endif
}

template <typename T>
T ToLargeEndian(T v) {
#if defined(TB_ARCH_ENDIAN_LARGE)
  return v;
#else
  T r;
  uint32 s = sizeof(T);
  char* data = (char*)&v;
  char* rdata = (char*)&r;
  for (int i = s - 1; i >= 0; i--) {
    *(rdata + (s - 1 - i)) = *(data + i);
  }
  return r;
#endif
}

TB_NAMESPACE_END

#endif // TB_BASE_BYTE_ORDER_HPP__
