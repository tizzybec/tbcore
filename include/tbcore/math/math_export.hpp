#ifndef TB_MATH_EXPORT_HPP_
#define TB_MATH_EXPORT_HPP_

#include "tbcore/predefined.hpp"

#ifdef TB_MATH_EXPORTS
# define TB_MATH_API  TB_API_EXPORT
#else
# define TB_MATH_API  TB_API_IMPORT
# define TB_LIB_NAME math
# include "tbcore/auto_link.hpp"
#endif

#endif //TB_MATH_EXPORT_HPP_
