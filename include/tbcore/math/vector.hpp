#ifndef TB_MATH_VECTOR_HPP__
#define TB_MATH_VECTOR_HPP__

#include "eigen/Core"

// usage: same as matrix

TB_NAMESPACE_BEGIN

// notice:column order storage

using Eigen::Vector2i;
using Eigen::Vector2f;
using Eigen::Vector2d;

using Eigen::Vector3i;
using Eigen::Vector3f;
using Eigen::Vector3d;

using Eigen::Vector4i;
using Eigen::Vector4f;
using Eigen::Vector4d;

using Eigen::VectorXi;
using Eigen::VectorXf;
using Eigen::VectorXd;

TB_NAMESPACE_END

#endif // TB_MATH_VECTOR_HPP__
