#ifndef TB_MATH_MATRIX_HPP__
#define TB_MATH_MATRIX_HPP__

#include "eigen/Core"

/**
	常用函数说明:
	
	声明:
	  Matrix3d a;
	
	初始化:
	  a << 1, 2, 3,
	       4, 5, 6,
	       7, 8, 9;
	
	获取矩阵元素:
	  a(0, 0), a(1, 2), a(3, 3)
	
	矩阵二元操作符:
	  a + b, a - b 
	
	一元操作符:
	  -a
	
	复合操作符:
	  a += b, a -= b
	
	标量操作:
	  a * scalar, scalar * a, a / scalar, a *= scalar, a /= scalar
	
	转置a^T:
	  a.transpose();
	note!!!: do not do this a = a.transpose();
	
	取复共轭:
	  a.conjugate();
	
	共轭转置:
	  a.adjoint();
	
	点积:
	  a.dot(b);
	
	向量积:
	  a.cross(b);
	
	元素和:
	  a.sum();
	
	元素积:
	  a.prod();
	
	元素平均值:
	  a.mean();
	
	元素最小值:
	  a.minCoeff();
	
	元素最大值:
	  a.maxCoeff();
	
	对角线系数和:
	  a.trace();
*/

TB_NAMESPACE_BEGIN

// notice:column order storage

using Eigen::Matrix2i;
using Eigen::Matrix2f;
using Eigen::Matrix2d;

using Eigen::Matrix3i;
using Eigen::Matrix3f;
using Eigen::Matrix3d;

using Eigen::Matrix4i;
using Eigen::Matrix4f;
using Eigen::Matrix4d;

using Eigen::MatrixXi;
using Eigen::MatrixXf;
using Eigen::MatrixXd;

TB_NAMESPACE_END

#endif // TB_MATH_MATRIX_HPP__
