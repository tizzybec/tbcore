#ifndef TB_BUILD_CONFIG_H_
#define TB_BUILD_CONFIG_H_

#include "git_commit_id.hpp"

#define TB_STRINGIZE(X) TB_DO_STRINGIZE(X)
#define TB_DO_STRINGIZE(X) #X

#define TB_CONCAT(A, B) TB_DO_CONCAT(A, B)
#define TB_DO_CONCAT(A, B) A##B

#define TB_VERSION_MAJOR 1

#define TB_VERSION_MIRROR 0

#define TB_VERSION_FIX 0

#define TB_VERSION_STR TB_STRINGIZE(TB_VERSION_MAJOR)"."\
  TB_STRINGIZE(TB_VERSION_MIRROR)"."TB_STRINGIZE(TB_VERSION_MIRROR)

#define BOOST_MOVE_USE_STANDARD_LIBRARY_MOVE

// disable dynamic cast
#define TB_CXX_NO_DYNAMIC_CAST

#if __cplusplus >= 201103L || _MSC_VER >= 1800
#define TB_CXX_HAS_TYPE_TRAIT
#define TB_CXX_HAS_VARIADIC_TEMPLATES
#define TB_CXX_PREFER_STD_LIB
#endif

// enable auto link mechanism
#define TB_USE_AUTO_LINK

// current build is static library
//#define TB_STATIC_LIB

// enable auto lib link diagnostic
#define TB_LIB_DIAGNOSTIC

#endif //TB_BUILD_CONFIG_H_
