#ifndef TB_GEO_COORDINATE_HPP__
#define TB_GEO_COORDINATE_HPP__

#include "tbcore/base/base.hpp"

#include "geo_export.hpp"

TB_NAMESPACE_BEGIN

typedef struct {
  double x;
  double y;
  double z;
  int32 orm;
} GCCCoord;

typedef struct {
  double x;
  double y;
  double z;
  int32 cell;
  int32 orm;
} GCSCoord;

typedef struct {
  double lon;
  double lat;
  double z;
  int32 orm;
} LLZCoord;

class TB_GEO_API Coord {
public:
	Coord();
	Coord(double lon, double lat, double height, int32 orm = 250);
	explicit Coord(const GCCCoord& coord);
	explicit Coord(const GCSCoord& coord);
	explicit Coord(const LLZCoord& coord);
	Coord(const Coord& rhs);
	Coord& operator=(const Coord& rhs);
	~Coord();
	double GetLon() const;
	void SetLon(double lon);
	double GetLat() const;
	void SetLat(double lat);
	double GetAlt() const;
	void SetAlt(double height);
  void SetLLZ(double lon, double lat, double z);
  void SetGCC(double x, double y, double z);
  void SetGCS(double x, double y, double z, int32 cell);
	const GCCCoord& AsGCC() const;
	const LLZCoord& AsLLZ() const;
	const GCSCoord& AsGCS() const;

private:
	struct CoordImpl;
	CoordImpl* impl_;
};

TB_NAMESPACE_END

#endif // TB_GEO_COORDINATE_HPP__
