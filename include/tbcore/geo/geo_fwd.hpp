#ifndef TB_GEO_GEO_FWD_HPP__
#define TB_GEO_GEO_FWD_HPP__

#include "tbcore/base/base.hpp"

TB_NAMESPACE_BEGIN

typedef boost::shared_ptr<Coord> CoordinatePtr;

TB_NAMESPACE_END

#endif // TB_GEO_GEO_FWD_HPP__
