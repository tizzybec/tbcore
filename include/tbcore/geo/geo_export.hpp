#ifndef TB_GEO_EXPORT_HPP__
#define TB_GEO_EXPORT_HPP__

#include "tbcore/predefined.hpp"

#ifdef TB_GEO_EXPORTS
# define TB_GEO_API TB_API_EXPORT
#else
# define TB_GEO_API TB_API_IMPORT
# define TB_LIB_NAME geo
# include "tbcore/auto_link.hpp"
#endif

#endif //TB_GEO_EXPORT_HPP__
