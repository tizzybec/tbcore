#ifndef TB_GEO_GEO_CONV_HPP__
#define TB_GEO_GEO_CONV_HPP__

#include "../base/result.hpp"
#include "../math/matrix.hpp"

#include "geo_export.hpp"
#include "coord.hpp"

TB_NAMESPACE_BEGIN

#define WGS84_SEMI_MAJOR_AXIS							6378137.0
#define WGS84_SEMI_MINOR_AXIS							6356752.3142
#define WGS84_RECIPROCAL_OF_FLATTENING		298.257223563
#define WGS84_FIRST_ECCENTRICITY_SQUARED	6.69437999014e-3
#define WGS84_SECOND_ECCENTRICITY_SQUARED	6.73949674228e-3

namespace geo {

//convert to llz coordinate
TB_GEO_API Result GCCToLLZ(const GCCCoord& src, LLZCoord& dst);
TB_GEO_API Result GCSToLLZ(const GCSCoord& src, LLZCoord& dst);

//convert to gcc coordinate
TB_GEO_API Result LLZToGCC(const LLZCoord& src, GCCCoord& dst);
TB_GEO_API Result GCSToGCC(const GCSCoord& src, GCCCoord& dst);

//convert to gcs coordinate
TB_GEO_API Result GCCToGCS(const GCCCoord& src, GCSCoord& dst);
TB_GEO_API Result LLZToGCS(const LLZCoord& src, GCSCoord& dst);

//direction cosine matrix ecef to enu
TB_GEO_API void ECEF2ENUMatrix(Matrix3d&, double lon, double lat);
TB_GEO_API void ECEF2ENUMatrix(Matrix3d&, const LLZCoord&);
TB_GEO_API void ECEF2ENUMatrix(Matrix3d&, const GCCCoord&);
TB_GEO_API void ECEF2ENUMatrix(Matrix3d&, const GCSCoord&);

//direction cosine matrix ecef to enu
TB_GEO_API void ENU2ECEFMatrix(Matrix3d&, double lon, double lat);
TB_GEO_API void ENU2ECEFMatrix(Matrix3d&, const LLZCoord&);
TB_GEO_API void ENU2ECEFMatrix(Matrix3d&, const GCCCoord&);
TB_GEO_API void ENU2ECEFMatrix(Matrix3d&, const GCSCoord&);

} //namespace geo

TB_NAMESPACE_END

#endif // TB_GEO_GEO_CONV_HPP__
