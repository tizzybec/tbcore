@echo off

echo ====== libevent ======

pushd third_party\src\libevent

nmake /f Makefile.nmake clean && nmake /f Makefile.nmake

xcopy include\event2\*.h %INCLUDE_ROOT_DIR%\event2\ /Y /S
xcopy WIN32-Code\event2\*.h %INCLUDE_ROOT_DIR%\event2\ /Y /S

xcopy libevent.lib %LIB_PLATFORM_DIR%\ /Y
xcopy libevent_core.lib %LIB_PLATFORM_DIR%\ /Y
xcopy libevent_extras.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd 