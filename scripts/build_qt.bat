@echo off

echo ====== qt ======

IF NOT DEFINED QT_SRC_ROOT (
	IF EXIST third_party\src\qt (
		set QT_SRC_ROOT=third_party\src\qt
		GOTO QT_SRC_CHECKED
	) ELSE (
		echo "QT_SRC_ROOT not configured"
	GOTO ERROR
	)
)

:QT_SRC_CHECKED

pushd %QT_SRC_ROOT%

set QT_ROOT_DIR=%CD%

set QMAKESPEC=%QT_ROOT_DIR%\qtbase\mkspecs\win32-msvc2015
echo Y | configure -mp -prefix %QT_ROOT_DIR%\build -debug-and-release -opensource -c++11 -shared -developer-build -make libs -make tools -skip qtwebkit -skip qtwebengine -skip qtwebkit-examples -skip qtquick1 -skip qtquickcontrols -skip qtsensors
IF %ERRORLEVEL% NEQ 0 GOTO ERROR
nmake
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

xcopy %QT_ROOT_DIR%\build %EXTRA_ROOT_DIR%\qt\ /Y /E

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd 