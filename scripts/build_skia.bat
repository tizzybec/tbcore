@echo off

echo ====== skia ======

pushd third_party\src\skia

set PATH=%PATH%;..\depot_tools

call python gyp_skia
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

set SKIA_OUT_DIR=
IF "%BUILD_TYPE%"=="Release" (
	set SKIA_OUT_DIR=Release
) ELSE (
	set SKIA_OUT_DIR=Debug
)
	
IF "%TARGET_ARCH%"=="64" (
	set SKIA_OUT_DIR=%SKIA_OUT_DIR%_x64
)

SET SKIA_OUT_DIR=out\%SKIA_OUT_DIR%

call ninja -C out\%SKIA_OUT_DIR% skia_lib
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

xcopy include\*.* %INCLUDE_ROOT_DIR%\skia\ /s /Y

xcopy %SKIA_OUT_DIR%\libpng_static.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_codec.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_core.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_effects.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_images.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_opts.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_opts_sse41.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_opts_ssse3.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_ports.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_sfnt.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_skgpu.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %SKIA_OUT_DIR%\skia_utils.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET ERRORLEVEL=%ERRORLEVEL%
echo ====== ERROR skia ======

:DONE

popd