@echo off

set START_TIME=%TIME%

:ALL

:MAPNIK
echo buiding mapnik...
call scripts\build_mapnik.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:MAPNIK
echo buiding boost...
call scripts\build_boost.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:ICU
echo buiding icu...
call scripts\build_icu.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:EIGEN
echo buiding EIGEN
call scripts\build_eigen.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:KDTREEPLUSPLUS
echo buiding kdtree++...
call scripts\build_kdtree++.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:LIBEVENT
echo buiding libevent...
call scripts\build_libevent.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:LUAJIT
echo buiding luajit...
call scripts\build_luajit.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:RAPIDJSON
echo buiding rapidjson...
call scripts\build_rapidjson.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:ROCKSDB
echo buiding rocksdb...
call scripts\build_rocksdb.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:SRM
echo buiding srm...
call scripts\build_srm.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:TBB
call scripts\build_tbb.bat
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

GOTO DONE

:ERROR
echo !!!!!ERROR: ABORTED!!!!!!
echo Started at %START_TIME%, finished at %TIME%

:DONE
echo -- DONE ---
echo Started at %START_TIME%, finished at %TIME%