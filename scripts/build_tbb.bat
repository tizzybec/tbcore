@echo off

echo ====== tbb ======

pushd third_party\src\tbb\build\vs2010

msbuild ^
.\makefile.sln ^
/t:%BUILD_TARGET% ^
/m:%NUMBER_OF_PROCESSORS% ^
/nologo ^
/p:Configuration=%BUILD_TYPE% ^
/p:BuildInParallel=true ^
/p:Platform=%BUILD_PLATFORM% ^
/p:PlatformToolset=%PLATFORM_TOOLSET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

IF NOT EXIST %INCLUDE_ROOT_DIR%\tbb (
	mkdir %INCLUDE_ROOT_DIR%\tbb
)

xcopy ..\..\include\tbb %INCLUDE_ROOT_DIR%\tbb /s /Y

set TBB_OUTPUT_DIR=
IF "%TARGET_ARCH%"=="64" (
	IF "%BUILD_TYPE%"=="Release" (
		set TBB_OUTPUT_DIR=intel64\Release
	) ELSE (
		set TBB_OUTPUT_DIR=intel64\Debug
	)
) ELSE (
	IF "%BUILD_TYPE%"=="Release" (
		set TBB_OUTPUT_DIR=ia32\Release
	) ELSE (
		set TBB_OUTPUT_DIR=ia32\Debug
	)
)

set TBB_FILE_SUFFIX=
IF "%BUILD_TYPE%"=="Debug" (
	set TBB_FILE_SUFFIX=_debug
)

xcopy %TBB_OUTPUT_DIR%\tbb%TBB_FILE_SUFFIX%.dll %LIB_PLATFORM_DIR%\ /Y
xcopy %TBB_OUTPUT_DIR%\tbb%TBB_FILE_SUFFIX%.lib %LIB_PLATFORM_DIR%\ /Y

xcopy %TBB_OUTPUT_DIR%\tbbmalloc%TBB_FILE_SUFFIX%.dll %LIB_PLATFORM_DIR%\ /Y
xcopy %TBB_OUTPUT_DIR%\tbbmalloc%TBB_FILE_SUFFIX%.lib %LIB_PLATFORM_DIR%\ /Y

xcopy %TBB_OUTPUT_DIR%\tbbmalloc_proxy%TBB_FILE_SUFFIX%.dll %LIB_PLATFORM_DIR%\ /Y
xcopy %TBB_OUTPUT_DIR%\tbbmalloc_proxy%TBB_FILE_SUFFIX%.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET ERRORLEVEL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd
