@echo off

echo ====== icu ======

pushd third_party\src\icu

IF EXIST %PATCHES%\icu-%ICU_VERSION%.diff ECHO patching with %PATCHES%\icu-%ICU_VERSION%.diff && patch -N -p1 < %PATCHES%/icu-%ICU_VERSION%.diff || %SKIP_FAILED_PATCH%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

set ICU_BIN_DIR=bin
set ICU_LIB_DIR=lib
IF "%TARGET_ARCH%" == "64" （
	set ICU_BIN=bin64
	set ICU_LIB=lib64
)

set ICU_OUTPUT_DLL_PREFIX=.dll
set ICU_OUTPUT_LIB_PREFIX=.lib
IF "%BUILD_TYPE%"=="Debug" (
	set ICU_OUTPUT_DLL_PREFIX=d.dll
	set ICU_OUTPUT_LIB_PREFIX=d.lib
)

msbuild ^
.\source\i18n\i18n.vcxproj ^
/t:%BUILD_TARGET% ^
/m:%NUMBER_OF_PROCESSORS% ^
/nologo ^
/p:Configuration=%BUILD_TYPE% ^
/p:BuildInParallel=true ^
/p:Platform=%BUILD_PLATFORM% ^
/p:PlatformToolset=%PLATFORM_TOOLSET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

msbuild ^
.\source\common\common.vcxproj ^
/t:Clean;Build ^
/m:%NUMBER_OF_PROCESSORS% ^
/nologo ^
/p:Configuration=%BUILD_TYPE% ^
/p:BuildInParallel=true ^
/p:Platform=%BUILD_PLATFORM% ^
/p:PlatformToolset=%PLATFORM_TOOLSET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

::ignore icucheck
::call .\source\allinone\icucheck.bat %ICU_PARAM_ARCH% ::%ICU_PARAM_CONFIGURATION%
::IF %ERRORLEVEL% NEQ 0 GOTO ERROR

xcopy include %INCLUDE_ROOT_DIR%\icu\ /Y /E

xcopy %ICU_BIN_DIR%\icudt%ICU_OUTPUT_PREFIX% %LIB_PLATFORM_DIR%\ /Y
xcopy %ICU_BIN_DIR%\icuin%ICU_OUTPUT_PREFIX% %LIB_PLATFORM_DIR%\ /Y
xcopy %ICU_BIN_DIR%\icuuc%ICU_OUTPUT_PREFIX% %LIB_PLATFORM_DIR%\ /Y

xcopy %ICU_LIB_DIR%\icudt%ICU_OUTPUT_LIB_PREFIX% %LIB_PLATFORM_DIR%\ /Y
xcopy %ICU_LIB_DIR%\icuin%ICU_OUTPUT_LIB_PREFIX% %LIB_PLATFORM_DIR%\ /Y
xcopy %ICU_LIB_DIR%\icuuc%ICU_OUTPUT_LIB_PREFIX% %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR icu ======

:DONE

popd