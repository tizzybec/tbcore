@echo off

echo ====== luajit ======

pushd third_party\src\luajit\src

call msvcbuild.bat

xcopy *.h %INCLUDE_ROOT_DIR%\luajit\ /Y /S
xcopy *.lua %INCLUDE_ROOT_DIR%\luajit\ /Y /S

xcopy luajit.exe %LIB_PLATFORM_DIR%\ /Y
xcopy lua51.dll %LIB_PLATFORM_DIR%\ /Y
xcopy lua51.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd 