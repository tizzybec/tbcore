@echo off

echo ====== srm ======

pushd third_party\src\srm

msbuild ^
.\src\lib\lib_srm_cpp_dynamic.vcxproj ^
/t:%BUILD_TARGET% ^
/m:%NUMBER_OF_PROCESSORS% ^
/nologo ^
/p:Configuration=%BUILD_TYPE% ^
/p:BuildInParallel=true ^
/p:Platform=%BUILD_PLATFORM% ^
/p:PlatformToolset=%PLATFORM_TOOLSET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

IF NOT EXIST %INCLUDE_ROOT_DIR%\srm (
	mkdir %INCLUDE_ROOT_DIR%\srm
)

set SRM_OUTPUT_DIR=
IF "%BUILD_TYPE%"=="Release" (
	set SRM_OUTPUT_DIR=lib\Release
) ELSE (
	set SRM_OUTPUT_DIR=lib\Debug
)

xcopy src\include\*.* %INCLUDE_ROOT_DIR%\srm\ /s /Y

xcopy %SRM_OUTPUT_DIR%\srm_cpp.dll %LIB_PLATFORM_DIR%\ /Y
xcopy %SRM_OUTPUT_DIR%\srm_cpp.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd