@echo off

echo ====== boost ======

pushd third_party\src\boost

set ICU_LINK=
IF "%BUILD_TYPE%"=="Release" (
	SET ICU_LINK=%LIB_PLATFORM_DIR%\icuuc.lib
) ELSE (
    SET ICU_LINK=%LIB_PLATFORM_DIR%\icuucd.lib
)

ECHO ICU_LINK %ICU_LINK%

if NOT EXIST b2.exe (
  echo calling bootstrap bat
  CALL bootstrap.bat --with-toolset=msvc-%TOOLS_VERSION%
  IF %ERRORLEVEL% NEQ 0 GOTO ERROR
)

IF %BUILD_TYPE% EQU Release (
  SET BOOST_BUILD_TYPE=release
) ELSE (
  SET BOOST_BUILD_TYPE=variant=debug debug-symbols=on debug-store=object
)

ECHO BOOST_BUILD_TYPE %BOOST_BUILD_TYPE%

CALL b2 -j%NUMBER_OF_PROCESSORS% ^
-d2 %BOOST_BUILD_TYPE% stage ^
--build-type=minimal ^
toolset=msvc-%TOOLS_VERSION% -q ^
runtime-link=shared ^
link=static ^
address-model=%BOOSTADDRESSMODEL% ^
--with-iostreams ^
--with-thread ^
--with-filesystem ^
--with-date_time ^
--with-system ^
--with-program_options ^
--with-chrono ^
--with-container ^
--with-regex ^
--with-atomic ^
--with-coroutine ^
--with-math ^
--with-random ^
--disable-filesystem2 ^
cxxflags="-DBOOST_MSVC_ENABLE_2014_JUN_CTP" ^
-sHAVE_ICU=1 ^
-sICU_PATH=%PKGDIR%\\icu ^
-sICU_LINK=%ICU_LINK% ^
-sZLIB_SOURCE=%PKGDIR%\zlib ^
-sBUILD=boost_unit_test_framework

IF %ERRORLEVEL% NEQ 0 GOTO ERROR

:: build boost_python now
:: we do this separately because
:: we want to dynamically link python

goto IGNORE_PYTHON_BUILD
CALL b2 -j%NUMBER_OF_PROCESSORS% ^
  -d2 %BOOST_BUILD_TYPE% stage ^
  --build-type=minimal toolset=msvc-%TOOLS_VERSION% -q ^
  runtime-link=shared link=shared ^
  cxxflags="-DBOOST_MSVC_ENABLE_2014_JUN_CTP" ^
  address-model=%BOOSTADDRESSMODEL% ^
  --with-python python=2.7
:IGNORE_PYTHON_BUILD
  
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

xcopy boost %INCLUDE_ROOT_DIR%\boost\ /Y /E
xcopy stage\lib %LIB_PLATFORM_DIR%\ /Y /E

IF %ERRORLEVEL% NEQ 0 GOTO ERROR

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd 