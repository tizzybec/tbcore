@echo off

echo ====== mapnik ======

pushd third_party\src\

IF "%BUILD_TYPE%"=="Debug" GOTO DONE

set MAPNIK_PLATFORM_DIR=
IF "%TARGET_ARCH%"=="64" (
	set MAPNIK_PLATFORM_DIR=mapnik-x64
) ELSE (
	set MAPNIK_PLATFORM_DIR=mapnik-x86
)

echo %MAPNIK_PLATFORM_DIR%

xcopy %MAPNIK_PLATFORM_DIR%\include %INCLUDE_ROOT_DIR%\ /S /Y /E

echo %INCLUDE_ROOT_DIR%

xcopy %MAPNIK_PLATFORM_DIR%\bin %BIN_PLATFORM_DIR%\ /S /Y /E
xcopy %MAPNIK_PLATFORM_DIR%\lib %LIB_PLATFORM_DIR%\ /S /Y /E
xcopy %MAPNIK_PLATFORM_DIR%\shared %SHARED_ROOT_DIR%\ /S /Y /E

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd 