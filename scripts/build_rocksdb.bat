@echo off

echo ====== rocksdb ======

pushd third_party\src\rocksdb

if EXIST build rd build /Q /S

mkdir build & cd build

IF "%TARGET_ARCH%"=="64" (
	cmake -G "Visual Studio 14 2015 Win64" ..
) else (
	cmake -G "Visual Studio 14 2015" ..
)

msbuild ^
.\rocksdb.vcxproj ^
/t:%BUILD_TARGET% ^
/m:%NUMBER_OF_PROCESSORS% ^
/nologo ^
/p:Configuration=%BUILD_TYPE% ^
/p:BuildInParallel=true ^
/p:Platform=%BUILD_PLATFORM% ^
/p:PlatformToolset=%PLATFORM_TOOLSET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR

cd ..

set ROCKSDB_OUTPUT_DIR=
IF "%BUILD_TYPE%"=="Release" (
	set ROCKSDB_OUTPUT_DIR=build\Release
) ELSE (
	set ROCKSDB_OUTPUT_DIR=build\Debug
)

xcopy include\rocksdb\*.* %INCLUDE_ROOT_DIR%\rocksdb\ /s /Y

xcopy %ROCKSDB_OUTPUT_DIR%\rocksdb.dll %LIB_PLATFORM_DIR%\ /Y
xcopy %ROCKSDB_OUTPUT_DIR%\rocksdb.lib %LIB_PLATFORM_DIR%\ /Y
xcopy %ROCKSDB_OUTPUT_DIR%\rocksdblib.lib %LIB_PLATFORM_DIR%\ /Y

GOTO DONE

:ERROR
SET EL=%ERRORLEVEL%
echo ====== ERROR tbb ======

:DONE

popd