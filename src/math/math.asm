ifndef X64
.MODEL flat, C
endif

.CODE

__asm_SinCos PROC
	fld		mmword ptr [ebp+8]
	fsincos
	mov		ecx, [ebp + 20]
	mov		edx, [ebp + 16]
	fstp	mmword ptr [ecx]
	fstp	mmword ptr [edx]
__asm_SinCos ENDP

__asm_SinCos64 PROC
	fld		mmword ptr [ebp + 8]
	fsincos
	mov		ecx, [ebp + 24]
	mov		edx, [ebp + 16]
	fstp	qword ptr [ecx]
	fstp	qword ptr [edx]
__asm_SinCos64 ENDP

__asm_FtoiFast PROC
	fld		mmword ptr [ebp+8]
	fistp	mmword ptr [ebp+16]	
__asm_FtoiFast ENDP

__asm_FtolFast PROC
	fld		mmword ptr [ebp+8]
	fistp	mmword ptr [ebp+16]	
__asm_FtolFast ENDP

END