#include "tbcore/base/datetime.hpp"

TB_NAMESPACE_BEGIN

DateTime::DateTime() {}

DateTime::DateTime( int64 milliseconds ) : value_(duration_type(milliseconds)) {}

DateTime::DateTime( const value_type& d ) : value_(d) {}

DateTime::DateTime( const Duration& d ) : value_(d.Value()) {}

DateTime::DateTime( const DateTime& rhs ) : value_(rhs.value_) {}

DateTime::DateTime( const duration_type& d ) : value_(d) {}

DateTime& DateTime::operator=( const DateTime& rhs ) {
  value_ = rhs.value_;
  return *this;
}

DateTime::~DateTime() {}

Duration DateTime::TimeSinceEpoch() {
  return Duration(value_.time_since_epoch().count());
}

DateTime DateTime::FromTimeT( std::time_t t ) {
  return DateTime(t * 1000);
}

DateTime DateTime::Now() NOEXCEPT
{
  return DateTime(clock_type::now().time_since_epoch().count());
}

DateTime DateTime::Min()
{
  return DateTime(value_type::min());
}

DateTime DateTime::Max()
{
   return DateTime(value_type::max());
}

DateTime& DateTime::operator+=(const Duration& d) {
  value_ += d.Value();
  return *this;
}

DateTime& DateTime::operator-=(const Duration& d) {
  value_ -= d.Value();
  return *this;
}

DateTime DateTime::operator+(const Duration& d) const {
  return DateTime(value_ + d.Value());
}

DateTime DateTime::operator-(const Duration& d) const {
  return DateTime(value_ - d.Value());
}

Duration DateTime::operator-(const DateTime& rhs) const {
  return Duration(value_ - rhs.value_);
}

int64 DateTime::TotalMilliseconds() const
{
  return value_.time_since_epoch().count();
}

double DateTime::TotalSeconds() const {
	return TotalMilliseconds() / 1000.0;
}

int DateTime::Compare(const DateTime& rhs) const {
	int64 diff = TotalMilliseconds() - rhs.TotalMilliseconds();
	return diff == 0 ? 0 : (diff > 0 ? 1 : -1);
}

TB_NAMESPACE_END