#include "testing/testing.hpp"

#include "datetime.hpp"

TB_NAMESPACE_BEGIN

TEST(DatetimeTest, BasicTest) {
	DateTime d(1000);
	EXPECT_EQ(d.TotalMilliseconds(), 1000);
	EXPECT_EQ(d.TotalSeconds(), 1);
	EXPECT_EQ(d.TimeSinceEpoch(), 1000);

	//2015-08-08 10:10:10
	//1438999810000 milliseconds from utc time(1970-01-01 00:00:00)
	struct tm tm1;
	memset(&tm1, 0, sizeof(tm1));
	tm1.tm_year = 2015 - 1900;
	tm1.tm_mon = 8 - 1;
	tm1.tm_mday = 8;
	tm1.tm_hour = 10;
	tm1.tm_min = 10;
	tm1.tm_sec = 10;
	std::time_t time1 = mktime(&tm1);

	d = DateTime(time1 * 1000);
	EXPECT_EQ(d.TotalMilliseconds(), 1438999810000);
	EXPECT_EQ(d.TotalSeconds(), 1438999810);
	EXPECT_EQ(d.TimeSinceEpoch(), Duration(1438999810000));

	DateTime d1 = DateTime::FromTimeT(time1);
	EXPECT_EQ(d, d1);

	DateTime nowdt = DateTime::Now();
	SUCCEED() << "now: " << nowdt.TotalMilliseconds();

	DateTime val;
	val = d + Duration(1000);
	EXPECT_EQ(val.TotalMilliseconds(), 1438999810000 + 1000);
	val = d - Duration(1000);
	EXPECT_EQ(val.TotalMilliseconds(), 1438999810000 - 1000);
	Duration vald = d - d;
	EXPECT_EQ(vald.TotalMilliseconds(), 0);

	DateTime selfop = d;
	selfop += Duration(1000);
	EXPECT_EQ(selfop.TotalMilliseconds(), 1438999810000 + 1000);
	selfop -=  Duration(1000);
	EXPECT_EQ(selfop.TotalMilliseconds(), 1438999810000);
}

TB_NAMESPACE_END