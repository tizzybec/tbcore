#include "tbcore/base/error_code.hpp"

#include <vector>

#include "tbcore/base/lazy.hpp"
#include "tbcore/base/mutex.hpp"

TB_NAMESPACE_BEGIN

struct ErrorCodeRecords {
  struct Item {
    int errorCode;
    String errorCodeStr;
    String errorDetail;
  };

  std::vector<Item> records_;
  SpinRWMutex recordsMutex_;
};

static Lazy<ErrorCodeRecords> sErrorCodeRecords;

String ErrorCode2String(ErrorCode& ec) {
  ErrorCodeRecords *v = sErrorCodeRecords.Value();
  SpinRWMutex::ScopedReadLock lock(v->recordsMutex_);
  for (const ErrorCodeRecords::Item &i : v->records_) {
    if (i.errorCode == ec) {
      return i.errorCodeStr;
    }
  }
	return _T("");
}

int String2ErrorCode(const String& str) {
  ErrorCodeRecords *v = sErrorCodeRecords.Value();
  SpinRWMutex::ScopedReadLock lock(v->recordsMutex_);
  for (const ErrorCodeRecords::Item i : v->records_) {
    if (i.errorCodeStr == str) {
      return i.errorCode;
    }
  }
  return kNoError;
}

const int RegisterErrorCode(const String& errCodeStr, const String& errDetail) {
  return 0;
}

String GetErrorDetail(const String& errCodeStr, const String& errDetail) {
  return _T("");
}

String GetErrorDetail(int errCode, const String& errDetail) {
  return _T("");
}

TB_NAMESPACE_END
