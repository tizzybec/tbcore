#include "tbcore/base/duration.hpp"

TB_NAMESPACE_BEGIN

Duration::Duration() {}

Duration::Duration( int64 milliseconds ) :value_(milliseconds) {}

Duration::Duration( const value_type& d ) :value_(d) {}

Duration::Duration( const Duration& rhs ) :value_(rhs.value_) {}

Duration& Duration::operator=( const Duration& rhs ) {
  value_ = rhs.value_;
  return *this;
}

Duration::~Duration() {}

double Duration::TotalSeconds() const {
  return value_.count() / 1E3;
}

int64 Duration::TotalMilliseconds() const {
  return value_.count();
}

void Duration::Reset( int64 milliseconds /*= 0*/ ) {
  value_ = value_type(milliseconds);
}

bool Duration::IsPositive() const {
  return value_.count() > 0;
}

Duration::rep_type Duration::Compare( const Duration& rhs ) const {
  return value_.count() - rhs.value_.count();
}

const Duration::value_type& Duration::Value() const {
  return value_;
}

bool Duration::IsZero() const {
  return value_.count() == 0;
}

Duration Duration::FromSeconds( uint64 seconds ){
  return Duration((int64)(seconds * 1E3));
}

Duration Duration::Min()
{
  return Duration((value_type::min)());
}

Duration Duration::Max()
{
  return Duration((value_type::max)());
}

Duration Duration::Zero()
{
  return Duration((value_type::zero)());
}

Duration& Duration::operator+=(const Duration& rhs)
{
  value_ += rhs.value_;
  return *this;
}

Duration& Duration::operator-=(const Duration& rhs)
{
  value_ -= rhs.value_;
  return *this;
}

Duration Duration::operator+(const Duration& rhs) const
{
  return Duration(value_.count() + rhs.value_.count());
}

Duration Duration::operator-(const Duration& rhs) const
{
  return Duration(value_.count() - rhs.value_.count());
}

Duration& Duration::operator*=(rep_type n)
{
  value_ *= n;
  return *this;
}

Duration& Duration::operator/=(rep_type n)
{
  value_ /= n;
  return *this;
}

Duration Duration::operator*(rep_type n) const
{
  return Duration(value_.count() * n);
}

Duration Duration::operator/(rep_type n) const
{
  return Duration(value_.count() / n);
}

bool Duration::IsNegtive() const {
	return value_.count() < 0;
}

TB_NAMESPACE_END