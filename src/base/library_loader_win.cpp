#include "tbcore/base/library_loader.hpp"

#include <Windows.h>

#include "tbcore/base/tostring.hpp"

TB_NAMESPACE_BEGIN

LibraryHandle LibraryLoader::Load(const TCHAR* path) {
	return ::LoadLibrary(path);
}

bool LibraryLoader::Free(LibraryHandle handle) {
	return ::FreeLibrary(handle) == TRUE;
}

SymbolAddress LibraryLoader::GetSymbol(LibraryHandle handle, const TCHAR* sym) {
  std::string sym_ = TB_ASCII_STRING(sym);
	return ::GetProcAddress(handle, sym_.c_str());
}

bool LibraryLoader::IsLoaded(const TCHAR* path) {
	return ::GetModuleHandle(path) != nullptr;
}

TB_NAMESPACE_END