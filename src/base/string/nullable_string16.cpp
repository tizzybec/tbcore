// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "tbcore/base/string/nullable_string16.hpp"

#include <ostream>

#include "tbcore/base/string/utf_string_conversions.hpp"

TB_NAMESPACE_BEGIN

std::ostream& operator<<(std::ostream& out, const NullableString16& value) {
  return value.is_null() ? out << "(null)" : out << UTF16ToUTF8(value.string());
}

TB_NAMESPACE_END
