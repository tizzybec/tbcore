#ifndef YASS_BASE_PATH_UTIL_HPP__
#define YASS_BASE_PATH_UTIL_HPP__

#include <vector>

#include "base_export.hpp"
#include "string.hpp"

YASS_NAMESPACE_BEGIN

class YASS_BASE_API Path {
public:
  static void Walk(const String &path,
    std::vector<String> &files, int depth = -1) NOEXCEPT;

  static void WalkWithExt(const String &path, const TCHAR *ext,
    std::vector<String> &files, int depth = -1) NOEXCEPT;

  static bool IsDir(const String &path) NOEXCEPT;

  static bool IsExists(const String &path) NOEXCEPT;

  static String Parent(const String &path) NOEXCEPT;

  static String ToNative(const String &path) NOEXCEPT;
};


YASS_NAMESPACE_END

#endif // YASS_BASE_PATH_UTIL_HPP__
