#include "tbcore/base/assert.hpp"

TB_NAMESPACE_BEGIN

static void DefaultSmartAssertHandler(const SmartAssert& a) { (void)a; }
static SmartAssertHandler SmartAssertHandler_ = DefaultSmartAssertHandler;

struct SmartAssert::SmartAssertImpl {
  typedef std::vector<std::pair<std::string, std::string> > value_vector;
  value_vector vals_;
  std::string expr_;
  std::string file_;
  int line_;
};

MSVC_PUSH_DISABLE_WARNING(4722)

SmartAssert::~SmartAssert() {
  SmartAssertHandler_(*this); 
  delete impl_;
  exit(-1);
}

MSVC_POP_WARNING()

SmartAssert& SmartAssert::PrintContext( const std::string& key, const std::string& val ) {
  impl_->vals_.push_back(std::make_pair(key, val));
  return *this;
}

void SmartAssert::Initialize(const std::string& expr, const char* file, int line) {
  impl_ = new SmartAssertImpl();
  impl_->expr_ = expr;
  impl_->file_ = file;
  impl_->line_ = line;
}

TB_BASE_API void SetAssertHandle( SmartAssertHandler handler ) {
  SmartAssertHandler_ = handler;
}

TB_NAMESPACE_END
