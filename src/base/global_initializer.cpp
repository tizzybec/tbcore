#include "tbcore/base/global_initializer.hpp"

TB_NAMESPACE_BEGIN

namespace detail {

Initializer gGlobalInitializer;

GlobalInitializerRegister::GlobalInitializerRegister(
	const std::string& name, 
	const InitializerContext::InitializerFunction& func, 
	const std::vector<std::string>& prerequisites, 
	const std::vector<std::string>& depends) {
	gGlobalInitializer.RegisterInitializer(name, func, prerequisites, depends);
}

} // namesapce detail


Result GlobalInitializer::Execute(const InitializerContext& context) {
  static bool inited = false;
  if (!inited) {
    inited = true;
    return detail::gGlobalInitializer.Execute(context);
  }
  return Result();
}

TB_NAMESPACE_END
