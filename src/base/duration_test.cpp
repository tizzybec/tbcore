#include "testing/testing.hpp"

#include "duration.hpp"

TB_NAMESPACE_BEGIN

TEST(DurationTest, BasicTest) {
	Duration d(1000);

	EXPECT_EQ(d.TotalMilliseconds(), 1000);
	EXPECT_EQ(d.TotalSeconds(), 1);
	EXPECT_TRUE(d.IsPositive());
	EXPECT_FALSE(d.IsNegtive());
	EXPECT_FALSE(d.IsZero());

	d.Reset(2000);

	EXPECT_EQ(d.TotalMilliseconds(), 2000);
	EXPECT_EQ(d.TotalSeconds(), 2);

	Duration d1(1000);

	EXPECT_TRUE(d != d1);
	EXPECT_TRUE(d == Duration(2000));
	EXPECT_TRUE(d <= Duration(2000));
	EXPECT_TRUE(d >= Duration(2000));
	EXPECT_TRUE(d < Duration(3000));
	EXPECT_TRUE(d <= Duration(3000));
	EXPECT_TRUE(d > Duration(1000));
	EXPECT_TRUE(d >= Duration(1000));

	EXPECT_LT(d1, d);
	Duration val = d + d1;
	EXPECT_EQ(val.TotalMilliseconds(), 3000);
	val = d - d1;
	EXPECT_EQ(val.TotalMilliseconds(), 1000);
	val = d * 2;
	EXPECT_EQ(val.TotalMilliseconds(), 4000);
	val = d / 2;
	EXPECT_EQ(val.TotalMilliseconds(), 1000);

	d += d1;
	EXPECT_EQ(d.TotalMilliseconds(), 3000);
	d -= d1;
	EXPECT_EQ(d.TotalMilliseconds(), 2000);
	d *= 2;
	EXPECT_EQ(d.TotalMilliseconds(), 4000);
	d /= 2;
	EXPECT_EQ(d.TotalMilliseconds(), 2000);

	Duration d2(0);

	EXPECT_TRUE(d2.IsZero());
	EXPECT_FALSE(d2.IsPositive());
	EXPECT_FALSE(d2.IsNegtive());

	Duration d3(-1000);

	EXPECT_FALSE(d3.IsZero());
	EXPECT_FALSE(d3.IsPositive());
	EXPECT_TRUE(d3.IsNegtive());

	Duration d4 = Duration::FromSeconds(1);
	EXPECT_EQ(d4.TotalMilliseconds(), 1000);
	EXPECT_EQ(d4.TotalSeconds(), 1);

	Duration mind = Duration::Min();
	Duration maxd = Duration::Max();
	Duration zerod = Duration::Zero();
	EXPECT_TRUE(zerod.IsZero());
}

TB_NAMESPACE_END