#include "tbcore/base/time_util.hpp"

TB_NAMESPACE_BEGIN

 void MillisecsToHMS( 
  int64 millisecs, 
  int32& hours, 
  int32& minutes, 
  int32& secs, 
  int32& millis ) {
  hours = (int32)(millisecs / 3600000);
  millisecs -= hours * 3600000;
  minutes = (int32)(millisecs / 60000);
  millisecs -= minutes * 60000;
  secs = (int32)(millisecs / 1000);
  millis = (int32)(millisecs - secs * 1000);
}

TB_NAMESPACE_END