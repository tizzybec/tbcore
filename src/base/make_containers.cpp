#include "tbcore/base/make_containers.hpp"

#include <stdarg.h>

TB_NAMESPACE_BEGIN

namespace detail {

std::vector<std::string> MakeStringVector(int ignored, ...) {
	va_list va;

	va_start(va, ignored);
	std::vector<std::string> result;
	const char* arg = NULL;
	while ((arg = va_arg(va, const char *)))
		result.push_back(arg);
	va_end(va);

	return result;
}

} // namespace detail

TB_NAMESPACE_END

