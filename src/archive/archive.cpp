#include "tbcore/archive/archive.hpp"

#include "tbcore/reflection/object.hpp"

TB_NAMESPACE_BEGIN

void SerVariantAsBinary(const Variant& val, MemoryBuffer& buf) {
	Archive<msgpack_protocol_tag>::Serialize(val, buf);
}

void DeserBinaryAsVariant(MemoryBuffer& data, Variant& val) {
	if (data.Size() == 0) {
		LERROR() << "empty data when pasrse binary data!";
		return;
	}

	val.Assign(Archive<msgpack_protocol_tag>::Deserialize(data));
}

void DeserBinaryAsVariant(const char* data, uint32 len, Variant& val) {
	if (len == 0) {
		LERROR() << "empty data when pasrse binary data!";
		return;
	}

	MemoryBuffer buff(data, len);
	val.Assign(Archive<msgpack_protocol_tag>::Deserialize(buff));
}

void SerVariantAsJson(const Variant& val, MemoryBuffer& buf) {
	Archive<json_protocol_tag>::Serialize(val, buf);
}

void DeserJsonAsVariant(MemoryBuffer& data, Variant& val) {
	if (data.Size() == 0) {
		LERROR() << "empty data when pasrse json document!";
		return;
	}

	val.Assign(Archive<json_protocol_tag>::Deserialize(data));
}

void DeserJsonAsVariant(const char* data, uint32 len, Variant& val) {
	if (len == 0) {
		LERROR() << "empty data when pasrse json document!";
		return;
	}

	MemoryBuffer buff(data, len);
	val.Assign(Archive<json_protocol_tag>::Deserialize(buff));
}

TB_NAMESPACE_END