#include "tbcore/reflection/unique_object.hpp"

TB_NAMESPACE_BEGIN

struct UniqueObject::UniqueObjectImpl {

};

UniqueObject::UniqueObject() : impl_(new UniqueObjectImpl()) {}

UniqueObject::~UniqueObject() {}

TB_IMPLEMENT_COPY_CONSTRUCTOR(UniqueObject);
TB_IMPLEMENT_ASSIGN_OPERATOR(UniqueObject);

const UniqueId& UniqueObject::GetObjectId() const {
	return data_.objId_;
}

void UniqueObject::SetObjectId(const UniqueId& objId) {
	data_.objId_ = objId;
}

TB_NAMESPACE_END