#include "reflection_traits.hpp"

using namespace TB::reflection;

//start test code
struct user_type_1 {};
struct user_type_2 {};
struct user_type_3 {};
struct user_type_4 {};
struct user_type_5 {};
struct user_type_6 {};
struct user_type_7 {};

typedef TypeList<user_type_1, user_type_2, user_type_3, user_type_4, user_type_5, user_type_6, user_type_7> SevenUserTypeList;
typedef TypeListSize<SevenUserTypeList> SevenUserListSize;
typedef TypeListAt<SevenUserTypeList, 0>::type UserType1;
typedef TypeListAt<SevenUserTypeList, 1>::type UserType2;
typedef TypeListAt<SevenUserTypeList, 2>::type UserType3;
typedef TypeListAt<SevenUserTypeList, 3>::type UserType4;
typedef TypeListAt<SevenUserTypeList, 4>::type UserType5;
typedef TypeListAt<SevenUserTypeList, 5>::type UserType6;
typedef TypeListAt<SevenUserTypeList, 6>::type UserType7;

BOOST_STATIC_ASSERT(( SevenUserListSize::size == 7 ));

BOOST_STATIC_ASSERT(( boost::is_same<user_type_1, UserType1>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_2, UserType2>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_3, UserType3>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_4, UserType4>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_5, UserType5>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_6, UserType6>::value ));
BOOST_STATIC_ASSERT(( boost::is_same<user_type_7, UserType7>::value ));

//end test code