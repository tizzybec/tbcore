#include "tbcore/reflection/signature.hpp"

#include "tbcore/base/assert.hpp"

#include "tbcore/reflection/type_traits.hpp"

TB_NAMESPACE_BEGIN

GivenTypeSignature::GivenTypeSignature() 
  : count_(0){
  types_[0] = GetVariantTypeId<void>();
}

GivenTypeSignature::GivenTypeSignature(uint32 retType) 
  : count_(0) {
  types_[0] = retType;
}

GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1) 
  : count_(1)  {
  types_[0] = retType;
  types_[1] = p1;
}

GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2) 
  : count_(2) {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
}

TB::GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2, uint32 p3) 
  : count_(3) {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
  types_[3] = p3;
}

GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2, uint32 p3, uint32 p4) 
  : count_(4) {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
  types_[3] = p3;
  types_[4] = p4;
}


GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2, uint32 p3, uint32 p4, uint32 p5) 
  : count_(5) {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
  types_[3] = p3;
  types_[4] = p4;
  types_[5] = p5;
}

GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2, uint32 p3, uint32 p4, uint32 p5, uint32 p6) 
  : count_(6) {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
  types_[3] = p3;
  types_[4] = p4;
  types_[5] = p5;
  types_[6] = p6;
}

GivenTypeSignature::GivenTypeSignature(uint32 retType, uint32 p1, 
  uint32 p2, uint32 p3, uint32 p4, uint32 p5, uint32 p6, uint32 p7) 
  : count_(7)  {
  types_[0] = retType;
  types_[1] = p1;
  types_[2] = p2;
  types_[3] = p3;
  types_[4] = p4;
  types_[5] = p5;
  types_[6] = p6;
  types_[7] = p7;
}

uint32 GivenTypeSignature::GetParamCount() const {
  return count_;
}

uint32 GivenTypeSignature::GetRetType() const {
  return types_[0];
}

uint32 GivenTypeSignature::GetParamType(uint32 index) const {
  TB_SMART_ASSERT(index >= 0 && index < 7);
  return types_[index + 1];
}

TB_NAMESPACE_END

