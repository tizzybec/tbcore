#include "tbcore/reflection/object.hpp"

#include "tbcore/reflection/variant.hpp"

TB_NAMESPACE_BEGIN

Object::Object() {}

Object::~Object() {}

Object::Object( const Object& rhs ) {}

Object& Object::operator=( const Object& rhs ) {
  return *this;
}

TB_NAMESPACE_END