#include "testing/testing.hpp"

#include "include/precompiled.hpp"
#include "reflection/register.hpp"
#include "reflection/object.hpp"

struct TestFunctorRegister : public TB_NAMESPACE::Object {
	DECLARE_reflection(TestFunctorRegister, TB_NAMESPACE::Object);
	void MembFunWith0ParamNoRet() {};
	void MembFunWith1ParamNoRet(int p1) {};
	void MembFunWith2ParamNoRet(int p1, int p2) {};
	void MembFunWith3ParamNoRet(int p1, int p2, int p3) {};
	void MembFunWith4ParamNoRet(int p1, int p2, int p3, int p4) {};
	void MembFunWith5ParamNoRet(int p1, int p2, int p3, int p4, int p5) {};
	void MembFunWith6ParamNoRet(int p1, int p2, int p3, int p4, int p5, int p6) {};
	void MembFunWith7ParamNoRet(int p1, int p2, int p3, int p4, int p5, int p6, int p7) {};

	int MembFunWith0ParamRet() { return 1; };
	int MembFunWith1ParamRet(int p1) { return 1; };
	int MembFunWith2ParamRet(int p1, int p2) { return 1; };
	int MembFunWith3ParamRet(int p1, int p2, int p3) { return 1; };
	int MembFunWith4ParamRet(int p1, int p2, int p3, int p4) { return 1; };
	int MembFunWith5ParamRet(int p1, int p2, int p3, int p4, int p5) { return 1; };
	int MembFunWith6ParamRet(int p1, int p2, int p3, int p4, int p5, int p6) { return 1; };
	int MembFunWith7ParamRet(int p1, int p2, int p3, int p4, int p5, int p6, int p7) { return 1; };
};

REGISTER_BEGIN(TestFunctorRegister)
REGISTER_FUNCTION(MembFunWith0ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith1ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith2ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith3ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith4ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith5ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith6ParamNoRet, "void")
REGISTER_FUNCTION(MembFunWith7ParamNoRet, "void")

REGISTER_FUNCTION(MembFunWith0ParamRet, "void")
REGISTER_FUNCTION(MembFunWith1ParamRet, "void")
REGISTER_FUNCTION(MembFunWith2ParamRet, "void")
REGISTER_FUNCTION(MembFunWith3ParamRet, "void")
REGISTER_FUNCTION(MembFunWith4ParamRet, "void")
REGISTER_FUNCTION(MembFunWith5ParamRet, "void")
REGISTER_FUNCTION(MembFunWith6ParamRet, "void")
REGISTER_FUNCTION(MembFunWith7ParamRet, "void")
REGISTER_END()

void PureFunWith0ParamNoRet() {};
void PureFunWith1ParamNoRet(int p1) {};
void PureFunWith2ParamNoRet(int p1, int p2) {};
void PureFunWith3ParamNoRet(int p1, int p2, int p3) {};
void PureFunWith4ParamNoRet(int p1, int p2, int p3, int p4) {};
void PureFunWith5ParamNoRet(int p1, int p2, int p3, int p4, int p5) {};
void PureFunWith6ParamNoRet(int p1, int p2, int p3, int p4, int p5, int p6) {};
void PureFunWith7ParamNoRet(int p1, int p2, int p3, int p4, int p5, int p6, int p7) {};

int PureFunWith0ParamRet() { return 1; };
int PureFunWith1ParamRet(int p1) { return 1; };
int PureFunWith2ParamRet(int p1, int p2) { return 1; };
int PureFunWith3ParamRet(int p1, int p2, int p3) { return 1; };
int PureFunWith4ParamRet(int p1, int p2, int p3, int p4) { return 1; };
int PureFunWith5ParamRet(int p1, int p2, int p3, int p4, int p5) { return 1; };
int PureFunWith6ParamRet(int p1, int p2, int p3, int p4, int p5, int p6) { return 1; };
int PureFunWith7ParamRet(int p1, int p2, int p3, int p4, int p5, int p6, int p7) { return 1; };