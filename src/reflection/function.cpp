#include "tbcore/reflection/function.hpp"
#include "tbcore/reflection/variant.hpp"

TB_NAMESPACE_BEGIN

namespace reflection {

Variant Function::operator()() const {
  if (GetArgCount() == 0) {
    Variant ret;
    if (HasRetValue()) {
      return InvokeWithRet(std::stack<Variant>());
    } else {
      InvokeWithoutRet(std::stack<Variant>());
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1) const {
  if (GetArgCount() == 1) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1, const Variant& p2) const {
  if (GetArgCount() == 2) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1, const Variant& p2, const Variant& p3) const {
  if (GetArgCount() == 3) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p3);
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

TB::Variant Function::operator()(const Variant& p1, const Variant& p2, const Variant& p3, const Variant& p4) const {
  if (GetArgCount() == 4) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p4);
    params.push(p3);
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1, const Variant& p2, const Variant& p3, const Variant& p4, const Variant& p5) const {
  if (GetArgCount() == 5) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p5);
    params.push(p4);
    params.push(p3);
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1, const Variant& p2, const Variant& p3, const Variant& p4, const Variant& p5, const Variant& p6) const {
  if (GetArgCount() == 6) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p6);
    params.push(p5);
    params.push(p4);
    params.push(p3);
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

Variant Function::operator()(const Variant& p1, const Variant& p2, const Variant& p3, const Variant& p4, const Variant& p5, const Variant& p6, const Variant& p7) const {
  if (GetArgCount() == 6) {
    Variant ret;
    std::stack<Variant> params;
    params.push(p7);
    params.push(p6);
    params.push(p5);
    params.push(p4);
    params.push(p3);
    params.push(p2);
    params.push(p1);
    if (HasRetValue()) {
      return InvokeWithRet(params);
    } else {
      InvokeWithoutRet(params);
    }
  } else {
    LERROR() << "argument count not matched!";
  }
  return Variant();
}

} // namespace reflection

TB_NAMESPACE_END