#include "tbcore/reflection/nobject.hpp"

#include "tbcore/reflection/variant.hpp"

TB_NAMESPACE_BEGIN

struct NObject::NObjectImpl {};

NObject::NObject() : impl_(new NObjectImpl()) {}
 
NObject::~NObject() {}

TB_IMPLEMENT_COPY_CONSTRUCTOR(NObject);

TB_IMPLEMENT_ASSIGN_OPERATOR(NObject);

const String& NObject::GetName() const {
  return data_.name_;
}

void NObject::SetName(const String& name) {
	data_.name_ = name;
}

TB_NAMESPACE_END