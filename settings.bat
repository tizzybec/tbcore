@echo off

set TARGET_ARCH=32
set NUMBER_OF_PROCESSORS=
set BUILD_TYPE=Release
set ROOTDIR=%~dp0
set TOOLS_VERSION=14.0
set PLATFORM_TOOLS_VERSION=4.0
SET RUNTIME_VERSION=vcredist-VS2015
SET YASS_GIT_REPO=https://git.oschina.net/tizzybec/tbcore.git
set PATCHES=%~dp0%\patches
set ICU_VERSION=56.1
set SKIP_FAILED_PATCH=1
set BUILD_TARGET=Clean;Build

:::::::::::::: OVERRIDE PARAMETERS
:NEXT-ARG

IF '%1'=='' GOTO ARGS-DONE
ECHO setting %1
SET %1
SHIFT
GOTO NEXT-ARG

:ARGS-DONE

IF DEFINED GIT_INSTALL_ROOT SET TEMP_GIT_DIR=%GIT_INSTALL_ROOT%&& GOTO TEST_FIND_GIT
IF EXIST "C:\Program Files (x86)\Git" SET TEMP_GIT_DIR=C:\Program Files (x86)\Git&& GOTO TEST_FIND_GIT
IF EXIST "C:\Program Files\Git" SET TEMP_GIT_DIR=C:\Program Files\Git&& GOTO TEST_FIND_GIT

:TEST_FIND_GIT
set ERRORLEVEL=0
SET PATH=%TEMP_GIT_DIR%\bin;%PATH%
SET PATH=%TEMP_GIT_DIR%\usr\bin;%PATH%
::find %USERPROFILE% -name "*.blabla"
git rev-parse HEAD 2>nul
IF %ERRORLEVEL% NEQ 0  (
    echo "git not found"
	GOTO ERROR
)
	
::git clone %YASS_GIT_REPO%
set BIN_ROOT_DIR=%ROOTDIR%3rd\bin
set LIB_ROOT_DIR=%ROOTDIR%3rd\lib
set INCLUDE_ROOT_DIR=%ROOTDIR%3rd\include
set SHARED_ROOT_DIR=%ROOTDIR%3rd\shared
set EXTRA_ROOT_DIR=%ROOTDIR%3rd\extra

IF "%TARGET_ARCH%"=="32" (
  SET BUILD_PLATFORM=Win32
  SET LIB_PLATFORM_DIR=%LIB_ROOT_DIR%\x86
  SET BIN_PLATFORM_DIR=%BIN_ROOT_DIR%\x86
  set BOOSTADDRESSMODEL=32
) ELSE (
  SET BUILD_PLATFORM=x64
  SET LIB_PLATFORM_DIR=%LIB_ROOT_DIR%\x64
  SET BIN_PLATFORM_DIR=%BIN_ROOT_DIR%\x64
  set BOOSTADDRESSMODEL=64
)

if "%TOOLS_VERSION%"=="14.0" (
  SET MSVC_VER=1900
  SET PLATFORM_TOOLSET=v140
  if "%TARGET_ARCH%"=="32" (
    CALL "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64_x86
  ) ELSE (
    CALL "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
  )
)

IF NOT EXIST %ROOTDIR%\3rd\include (
    mkdir %ROOTDIR%\3rd\include
)

IF NOT EXIST %ROOTDIR%\3rd\lib (
    mkdir %ROOTDIR%\3rd\lib
)

IF NOT EXIST %BIN_PLATFORM_DIR% (
    mkdir %BIN_PLATFORM_DIR%
)

IF NOT EXIST %LIB_PLATFORM_DIR% (
    mkdir %LIB_PLATFORM_DIR%
)

IF "%BUILD_TYPE%"=="Debug" (
    set BIN_PLATFORM_DIR=%BIN_PLATFORM_DIR%\debug
    set LIB_PLATFORM_DIR=%LIB_PLATFORM_DIR%\debug
) ELSE (
    set BIN_PLATFORM_DIR=%BIN_PLATFORM_DIR%\release
    set LIB_PLATFORM_DIR=%LIB_PLATFORM_DIR%\release
)

IF NOT EXIST %BIN_PLATFORM_DIR% (
    mkdir %BIN_PLATFORM_DIR%
)

IF NOT EXIST %LIB_PLATFORM_DIR% (
    mkdir %LIB_PLATFORM_DIR%
)

IF %ERRORLEVEL% NEQ 0  (
	GOTO ERROR
) ELSE (
	GOTO DONE
)

:ERROR
ECHO !!!!!!!! ===== ERROR ==== !!!!!!!!
ECHO builds cannot be run unless settings.bat finished successfully

:DONE
echo TARGET_ARCH^: %TARGET_ARCH%
echo TOOLS_VERSION^: %TOOLS_VERSION%
echo NUMBER_OF_PROCESSORS^: %NUMBER_OF_PROCESSORS%
echo BUILD_TYPE^: %BUILD_TYPE%
echo TOOLS_VERSION^: %TOOLS_VERSION%

echo. && echo building within %ROOTDIR% && ECHO. &&ECHO.

echo ------ USAGE ------
echo Calling "scripts\build" will run with above default parameters.
echo Parameters can be overriden, see top of source of this file for
echo overridable parameters. && ECHO.
echo Override like this (parameters MUST be quoted!)^: && ECHO.
echo settings.bat "TARGET_ARCH=64" "BUILD_TYPE=Debug"
echo.