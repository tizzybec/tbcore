--
-- tbcore 1.0 build script 
--

---------------------------------------------------------------------------

workspace "all"
  platforms { "x86", "x86_64" }
  configurations { "debug", "release" }
  location "build"

  if os.istarget("windows") then
    defines { "WIN32" }
  end
  
rule "AsmRule"
  location "build"
  display "asm compiler"
  fileextension ".asm"
  buildmessage 'ml %(FullPath)...'
  buildcommands 'ml.exe /c /Fo $(IntDir)%(Filename)_asm.obj /Zi %(FullPath)'
  buildoutputs { '$(IntDir)%(Filename)_asm.obj' } 

---------------------------------------------------------------------------

project "tbcore"
  kind "SharedLib"
  language "C++"
  targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}"
  objdir "%{cfg.location}/%{cfg.platform}/%{cfg.buildcfg}"
  rules { "AsmRule" }
  
  defines 
  {
    "TB_BASE_EXPORTS", 
    "TB_GEO_EXPORTS", 
    "TB_MATH_EXPORTS", 
    "TB_ARCHIVE_EXPORTS", 
    "TB_NET_EXPORTS", 
    "TB_DB_EXPORTS", 
    "TB_UTIL_EXPORTS", 
    "TB_REFLECTION_EXPORTS", 
    "WIN32",
   }
  
  filter { "platforms:x86" }
    architecture "x86"
  filter { }
  
  filter { "platforms:x86_64" }
    architecture "x86_64"
  filter { }
  
  filter { "configurations:Debug" }
    defines { "DEBUG", "_DEBUG"}
    runtime "Debug"
  filter { }
    
  filter "configurations:release"
    defines { "NDEBUG" }
    optimize "Speed"
    runtime "Release"
  filter {}
  
  files 
  { 
    "./src/**.hpp", 
    "./src/**.cpp", 
    "./src/**.cc", 
    "./src/**.asm", 
    "./include/**.hpp",
  }

  libdirs 
  { 
    "./3rd/lib/%{cfg.platform}/%{cfg.buildcfg}"
  }
    
  excludes 
  { 
    "**_test.cpp",
    "**_unittest.cpp",
    "**_posix.cpp",
    "**_posix.cc",
    "**_test.cc",
    "**_unittest.cc",
   }
    
  includedirs 
  {
    "./3rd/include",
    "./include"
  }
   
  links 
  { 
    "srm_cpp.lib", 
    "rocksdb.lib",
    "lua51.lib"
  }

---------------------------------------------------------------------------

project "tbcore_test"
  kind "ConsoleApp"
  
  files 
  { 
    "**_test.cpp", 
    "src/gtest/gmock/src/gmock_main.cc"
  }